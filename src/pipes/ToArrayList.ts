import { Injectable, Pipe } from '@angular/core';

/*
  Generated class for the ToArrayList pipe.

  See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
  Angular 2 Pipes.
*/
@Pipe({
  name: 'ToArrayList'
})
@Injectable()
export class ToArrayList {
  /*
    Takes a value and makes it lowercase.
   */
  transform(value) : any {
    let objs = [];
    for (let key in value) {
      let obj = {key: key, val: value[key]};
      objs.push(obj);
    }
    console.log(objs);
    return objs;
  }
}
