import { Component, ViewChild } from '@angular/core';
import {Platform, MenuController, Nav, App, Events, ModalController, AlertController} from 'ionic-angular';
import {AngularFire} from 'angularfire2';
import { StatusBar, Splashscreen, Keyboard } from 'ionic-native';
import moment from 'moment';
import 'moment/locale/pt'


import {FirebaseData} from "../providers/firebase-data";
import { WalkthroughPage } from '../pages/walkthrough/walkthrough';
import { EuProfilePage } from '../pages/eu-profile/eu-profile';
import { ContactPage } from '../pages/contact/contact';
import { MessagesPage } from '../pages/messages/messages';
import { MessagesDetailPage } from '../pages/messages/messages-detail';
import { ProductPage } from '../pages/product/product';
import { VoucherPage } from '../pages/voucher/voucher';
import { PromotionPage } from '../pages/promotion/promotion';
import {ProgramDetailPage} from "../pages/program-detail/program-detail";
import {TermsOfServicePage} from "../pages/terms-of-service/terms-of-service";
import {Session} from "../providers/session";
import {AlertLoader} from "../providers/alert-loader";


declare var FCMPlugin;

@Component({
  selector: 'app-root',
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  // make WalkthroughPage the root (or first) page
  rootPage: any = WalkthroughPage;
  userid:string;

  pages: Array<{title: string, icon: string, component: any, size:string, ispush:any}>;
  pushPages: Array<{title: string, icon: string, component: any, size:string}>;

  constructor(
    platform: Platform,
    public menu: MenuController,
    public app: App,
    public events: Events,
    public modalCtrl: ModalController,
    public alertCtrl: AlertController,
    public session: Session,
    public alertLoader:AlertLoader,
    public fb: FirebaseData,
    private af: AngularFire
  ) {

    moment.locale('pt');

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      Splashscreen.hide();
      StatusBar.styleDefault();
      Keyboard.hideKeyboardAccessoryBar(false);
    });


    // Left side menu content
    this.pages = [
      { title: 'EU', icon: './assets/images/menu/eu_icon.png', component: EuProfilePage, size:'22', ispush:true },
      { title: 'Mensagens', icon: './assets/images/menu/mesage_icon.png', component: MessagesPage, size:'30', ispush:true },
      { title: 'ALLERGAN E PRODUTOS', icon: './assets/images/menu/product_icon.png', component: ProductPage, size:'15', ispush:true },
      { title: 'Promoção', icon: './assets/images/menu/promo_icon.png', component: EuProfilePage, size:'', ispush:'modal' },
      { title: 'Nº DE VOUCHER', icon: './assets/images/menu/voucher_icon.png', component: VoucherPage, size:'', ispush:true },
      { title: 'Contato', icon: './assets/images/menu/contact_icon.png', component: ContactPage, size:'', ispush:true },
      { title: 'TERMOS DE USO', icon: './assets/images/menu/terms_icon.png', component: TermsOfServicePage, size:'15', ispush:true },
      { title: 'SAIR', icon: './assets/images/menu/logodff_icon.png', component: WalkthroughPage, size:'17', ispush:'logout' }
    ];
    // events.subscribe('login:logined', (status) => {
    //   if (status) {
    //     this.pages.unshift({ title: 'EU', icon: './assets/images/menu/eu_icon.png', component: EuProfilePage, size:'22', ispush:true });
    //   } else {
    //     if (this.pages.length > 7) this.pages.shift();
    //   }
    // });


    if (typeof (FCMPlugin) !== "undefined") {
      FCMPlugin.onNotification((data) => {
        this.userid = this.session.getUserId();
        console.log(data);
        if(data.wasTapped){
          console.log(data);
          this.nav.push(MessagesPage);
        } else {
          if(data.notificaion_type == "check_in"){
            let lectureId = data.lecture_id;
            this.fb.object('lectures/'+lectureId).take(1).subscribe((lecture:any)=>{
              let alert = this.alertCtrl.create({
                title: 'Check In',
                subTitle: 'Você quer fazer o check in na palestra '+lecture.name+'?',
                mode:'ios',
                buttons: [
                  {
                    text: 'NÃO',
                    role: 'cancel',
                    handler: () => {
                      if(!lecture.uncheck_in_doctor){
                        lecture.uncheck_in_doctor = [];
                      }
                      if(!lecture.check_in_doctor){
                        lecture.check_in_doctor = [];
                      }
                      if(lecture.check_in_doctor.indexOf(this.userid) != -1){
                        lecture.check_in_doctor.splice(lecture.check_in_doctor.indexOf(this.userid), 1);
                      }
                      lecture.uncheck_in_doctor.push(this.userid);
                      this.fb.update('lectures/'+lectureId, lecture);
                    }
                  },
                  {
                    text: 'SIM',
                    handler: () => {
                      if(!lecture.check_in_doctor){
                        lecture.check_in_doctor = [];
                      }
                      lecture.check_in_doctor.push(this.userid);
                      this.fb.update('lectures/'+lectureId, lecture);
                      this.nav.push(ProgramDetailPage, {'key': lectureId});
                    }
                  }
                ]
              });
              alert.present();
            })
          }else{
            if(data.lecture_id){
              this.alertLoader.complexAlert('Temos um novo update para você! , OK', ()=>{
                this.nav.push(ProgramDetailPage, {'key': data.lecture_id});
              });
            }else{
              this.alertLoader.complexAlert('Nova Mensagem, você recebeu uma nova notificação, OK', ()=>{
                this.nav.push(MessagesDetailPage, {item: data});
              });
            }
          }
        }
      }, error=>{
        console.log("error", error);
      });
    }

  }

  openPage(page) {
    if (page.ispush === 'logout') {
      let alert = this.alertCtrl.create({
        title: 'Sair',
        subTitle: 'Você tem certeza que deseja sair?',
        mode:'ios',
        buttons: [
          {
            text: 'NÃO',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
              this.menu.close();
            }
          },
          {
            text: 'SIM',
            handler: () => {
              this.session.clear();
              this.af.auth.logout();
              setTimeout(_ => {
                this.menu.close();
                this.app.getRootNav().push(page.component)
              }, 1500);
            }
          }
        ]
      });
      alert.present();
    } else {
      if (page.ispush === 'modal'){
        let modal = this.modalCtrl.create(PromotionPage, { 'content': {type:'image/jpeg',file: 'https://firebasestorage.googleapis.com/v0/b/allergan-eventos.appspot.com/o/images%2FEqFyxLsCKDuy33qyDn9z_1492529572529?alt=media&token=d235181d-401d-4417-8dc7-6807ba20e77d'} });
        modal.onDidDismiss(data => {
          console.log(data);
        });
        modal.present();
      } else {
        // close the menu when clicking a link from the menu
        this.menu.close();
        // navigate to the new page if it is not the current page
        if (page.ispush) this.app.getRootNav().push(page.component)
        else this.nav.setRoot(page.component);
      }
    }


  }

  pushPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // rootNav is now deprecated (since beta 11) (https://forum.ionicframework.com/t/cant-access-rootnav-after-upgrade-to-beta-11/59889)
    this.app.getRootNav().push(page.component);
  }
}
