// Core Libs
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { LocalStorageModule } from 'angular-2-local-storage';
// Main Pages
import { NotificationsPage } from '../pages/notifications/notifications';
import { TabsNavigationPage } from '../pages/tabs-navigation/tabs-navigation';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { TermsOfServicePage } from '../pages/terms-of-service/terms-of-service';
import { PrivacyPolicyPage } from '../pages/privacy-policy/privacy-policy';
import { WalkthroughPage } from '../pages/walkthrough/walkthrough';
import { LoginPage } from '../pages/login/login';
import { SignupPage } from '../pages/signup/signup';
import { HomePage } from '../pages/home/home';
import { EuProfilePage } from '../pages/eu-profile/eu-profile';
import { ContactPage } from '../pages/contact/contact';
import { MessagesPage } from '../pages/messages/messages';
import { MessagesDetailPage } from '../pages/messages/messages-detail';
import { ProductPage } from '../pages/product/product';
import { EventosPage } from '../pages/eventos/eventos';
import { EventDetailPage } from '../pages/event-detail/event-detail';
import { EventInfoPage } from '../pages/event-info/event-info';
import { EventEnquetPage } from '../pages/event-enquet/event-enquet';
import { EnqueteListPage } from '../pages/enquete-list/enquete-list';
import { SciencePage } from '../pages/science/science';
import { ScienceDetailPage } from '../pages/science-detail/science-detail';
import { ScienceInfoPage } from '../pages/science-info/science-info';
import { ProgramingPage } from '../pages/programing/programing';
import { AddPopoverPage } from '../pages/programing/add-popover';
import { ProgramDetailPage } from '../pages/program-detail/program-detail';
import { UsersListPage } from '../pages/users-list/users-list';
import { TravelAgencyPage } from '../pages/travel-agency/travel-agency';
import { AdminTestPage } from '../pages/admin-test/admin-test';
import { VideosListPage } from '../pages/videos-list/videos-list';
import { FileModalPage } from '../pages/videos-list/file-modal';
import { SpeakerDetailPage } from '../pages/speaker-detail/speaker-detail';
import { VoucherPage } from '../pages/voucher/voucher';
import { PromotionPage } from '../pages/promotion/promotion';
import { PathMapPage } from '../pages/path-map/path-map';
// Components
import { PreloadImage } from '../components/preload-image/preload-image';
import { BackgroundImage } from '../components/background-image/background-image';
import { ShowHideContainer } from '../components/show-hide-password/show-hide-container';
import { ShowHideInput } from '../components/show-hide-password/show-hide-input';
import { ColorRadio } from '../components/color-radio/color-radio';
import { CounterInput } from '../components/counter-input/counter-input';
import { Rating } from '../components/rating/rating';
import { DurationLabel } from '../components/duration-label/duration-label';
// Providers
import { NotificationsService } from '../pages/notifications/notifications.service';
import { FirebaseAuth } from '../providers/firebase-auth';
import { FirebaseData } from '../providers/firebase-data';
import { FirebaseStorage } from '../providers/firebase-storage';
import { BaseData } from '../providers/base-data';
import { Session } from '../providers/session';
import { Tools } from '../providers/tools';
import { Utilities } from '../providers/utilities';
import { AlertLoader } from '../providers/alert-loader';
// Pipes
import {ToArrayList} from '../pipes/ToArrayList';
import {SafeHtmlPipe} from '../pipes/SafeHtml';
// Firebase config
export const firebaseConfig = {
  apiKey: "AIzaSyCGWN6DasmE31XypavULqLCWY4yeEUgU2U",
  authDomain: "allergan-eventos.firebaseapp.com",
  databaseURL: "https://allergan-eventos.firebaseio.com",
  projectId: "allergan-eventos",
  storageBucket: "allergan-eventos.appspot.com",
  messagingSenderId: "733130961792"
};

@NgModule({
  declarations: [
    NotificationsPage,
    TabsNavigationPage,
    ForgotPasswordPage,
    TermsOfServicePage,
    PrivacyPolicyPage,
    MyApp,
    WalkthroughPage,
    LoginPage,
    SignupPage,
    HomePage,
    EuProfilePage,
    ContactPage,
    MessagesPage,
    MessagesDetailPage,
    ProductPage,
    EventosPage,
    SciencePage,
    EventDetailPage,
    EventInfoPage,
    ProgramingPage,
    AddPopoverPage,
    ProgramDetailPage,
    ScienceDetailPage,
    ScienceInfoPage,
    UsersListPage,
    TravelAgencyPage,
    EventEnquetPage,
    EnqueteListPage,
    AdminTestPage,
    VideosListPage,
    FileModalPage,
    SpeakerDetailPage,
    VoucherPage,
    PromotionPage,
    PathMapPage,

    PreloadImage,
    BackgroundImage,
    ShowHideContainer,
    ShowHideInput,
    ColorRadio,
    CounterInput,
    Rating,
    DurationLabel,
    ToArrayList,
    SafeHtmlPipe
  ],
  imports: [
    AngularFireModule.initializeApp(firebaseConfig),
    // Local storage config
    LocalStorageModule.withConfig({
      prefix: 'allergan-v2',
      storageType: 'localStorage'
    }),
    IonicModule.forRoot(MyApp, {
      backButtonText: 'VOLTAR',
      backButtonIcon: 'ios-arrow-back',
      iconMode: 'ios',
      pageTransition: 'ios'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    NotificationsPage,
    TabsNavigationPage,
    ForgotPasswordPage,
    TermsOfServicePage,
    PrivacyPolicyPage,

    MyApp,
    WalkthroughPage,
    LoginPage,
    SignupPage,
    EuProfilePage,
    HomePage,
    ContactPage,
    MessagesPage,
    MessagesDetailPage,
    ProductPage,
    EventosPage,
    SciencePage,
    EventDetailPage,
    EventInfoPage,
    ProgramingPage,
    AddPopoverPage,
    ProgramDetailPage,
    ScienceDetailPage,
    ScienceInfoPage,
    UsersListPage,
    TravelAgencyPage,
    EventEnquetPage,
    EnqueteListPage,
    AdminTestPage,
    VideosListPage,
    FileModalPage,
    SpeakerDetailPage,
    VoucherPage,
    PromotionPage,
    PathMapPage
  ],
  providers: [
    NotificationsService,
    FirebaseAuth,
    FirebaseData,
    FirebaseStorage,
    Utilities,
    Tools,
    Session,
    AlertLoader,
    BaseData
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
