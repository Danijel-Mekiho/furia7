import {Component, ViewChild, ChangeDetectorRef} from '@angular/core';
import {
  NavController, NavParams, Content, ModalController, LoadingController, AlertController,
  Platform
} from 'ionic-angular';
import moment from 'moment';
import _ from 'lodash';

import {FileModalPage} from './file-modal';
import {FirebaseData} from "../../providers/firebase-data";
import {FirebaseAuth} from "../../providers/firebase-auth";
import {AngularFire} from "angularfire2";
import {BaseData} from "../../providers/base-data";
import {HomePage} from '../home/home';
import {Session} from "../../providers/session";
import {Utilities} from "../../providers/utilities";

declare var cordova;
/*
 Generated class for the VideosList page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-videos-list',
  templateUrl: 'videos-list.html'
})
export class VideosListPage {
  @ViewChild(Content) content: Content;
  isScroll: boolean;
  data: any;
  originalData:any;
  currentIndex: number;
  userid: string;
  userVouchers: any;
  allVouchers: any;
  searchType:any = "";
  searchInput:string = "";


  constructor(public platform: Platform,
              public navCtrl: NavController,
              public navParams: NavParams,
              private cref: ChangeDetectorRef,
              public baseData: BaseData,
              public fb: FirebaseData,
              public modalCtrl: ModalController,
              public auth: FirebaseAuth,
              public af: AngularFire,
              public loadingCtrl: LoadingController,
              public session: Session,
              public utilities: Utilities,
              public alertCtrl: AlertController,) {
    this.isScroll = true;
    this.userid = session.getUserId();
    this.allVouchers = this.baseData.getVouchers();
    console.log('allVouchers', this.allVouchers);
    let loading = this.loadingCtrl.create();
    loading.present();

    utilities.getCongressAndEventIdsByUserId(this.userid).subscribe(eventsIds => {
      let user_vouchers = this.af.database.object('users/' + this.userid);
      user_vouchers.take(1).subscribe(user => {
        this.userVouchers = user.vouchers || [];
        this.fb.list('materials').take(1).subscribe(snapshot => {
          let currentTime = new Date().getTime();
          let userMaterials = _.filter(snapshot, (item:any) => {

            //check with voucher
            if(_.indexOf(this.userVouchers, item.voucher_id) != -1){
              let selVoucher:any = _.find(this.allVouchers, {'voucher':item.voucher_id});
              if(!selVoucher || (selVoucher.start_time <= currentTime && selVoucher.end_time >= currentTime)){
                return true;
              }
            }
            //check with events
            if(_.indexOf(eventsIds, item.event_id) != -1){
              return true;
            }

            //if not select eventid or voucher
            if((!item.voucher_id || item.voucher_id=="No Select") && (!item.event_id || item.event_id=="No Select")){
              console.log(item);
              return true;
            }

            return false;
          })

          userMaterials = _.forEach(userMaterials, (item)=>{
            if(!item.created_at){
              item.created_at = 0;
            }
          });

          this.originalData = _.orderBy(userMaterials, ['created_at'],['desc']);






          this.data =this.originalData

          setTimeout(_ => {
            loading.dismiss();
            this.scrollChange();
          }, 1000);
        });
      })
    })
  }

  checkVoucher(voucher) {
    let isVoucher = false;
    for (let i = 0; i < this.userVouchers.length; i++) {
      if (this.userVouchers[i] === voucher) {
        let selVoucher = null;
        for (let j = 0; j < this.allVouchers.length; j++) {
          let vouitem = this.allVouchers[j];
          if (vouitem.voucher === voucher) {
            selVoucher = vouitem;
            break;
          }
        }
        if (selVoucher !== null) {
          let startTime = selVoucher.start_time;
          let endTime = selVoucher.end_time;
          let currentTime = new Date().getTime();
          if (startTime < currentTime && endTime > currentTime) {
            isVoucher = true;
          }
        }
        break;
      }
    }
    if (!voucher) isVoucher = true;
    return isVoucher;
  }

  goHome() {
    this.navCtrl.setRoot(HomePage)
  }

  getValidTimeRange(key) {
    return moment(key).format('D/MM/YY HH:mm');
  }

  gotoMaterial(content) {
    if (content.type === 'video/mp4') {
      this.platform.ready().then(() => {
        let options = {
          toolbar: 'no',
        };
        console.log(options, content);
        cordova.InAppBrowser.open(content.file, '_blank');
      });
      /*let modal = this.modalCtrl.create(FileModalPage, {'content': content});
      modal.onDidDismiss(data => {
        console.log(data);
      });
      modal.present();
      */
    } else if(content.type === 'image/jpeg'){
      let modal = this.modalCtrl.create(FileModalPage, {'content': content});
       modal.onDidDismiss(data => {
       console.log(data);
       });
       modal.present();
    }else {

      let alert = this.alertCtrl.create({
        title: 'Arquivo Download',
        subTitle: 'Você confirma o download do arquivo ' + content.filename,
        mode: 'ios',
        buttons: [{
          text: 'SIM',
          handler: () => {
            this.platform.ready().then(() => {
              if(this.platform.is("android")){
                let url = 'https://docs.google.com/viewer?url='+encodeURIComponent(content.file) + '&embedded=true';
                cordova.InAppBrowser.open(url, '_blank');
              }else{
                cordova.InAppBrowser.open(content.file, '_blank');
              }
            });
          }
        }, {
          text: 'NÃO',
          handler: () => {

          }
        }]
      });
      alert.present();
      // alert(content.filename + ' is ' + content.type + ' file.')
    }
    // this.navCtrl.push(EventEnquetPage, {key: key, index: index});
  }

  scrollTo(element: string) {
    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 500);
    this.scrollChange();
  }

  scrollChange() {
    let dementions = this.content.getContentDimensions();
    if (dementions.scrollTop > 150) {
      this.isScroll = true;
    } else {
      this.isScroll = false;
    }
    this.cref.detectChanges();
    setTimeout(_ => {
      this.content.resize();
    }, 120);
  }

  onChangeSearchInput(){
    let data = this.originalData;
    if(this.searchType && this.searchType.length>0){
      data = _.filter(data, (item:any)=>{
        if(this.searchType.indexOf(item.type) != -1){
          return true;
        }else{
          return false;
        }
      });
    }
    if(this.searchInput){
      data = _.filter(data, (item:any)=>{
        if(item.desc && item.desc.toLocaleLowerCase().search(this.searchInput.toLocaleLowerCase()) != -1){
          return true;
        }else if(item.filename && item.filename.toLocaleLowerCase().search(this.searchInput.toLocaleLowerCase()) != -1){
          return true;
        }else{
          return false;
        }
      });
    }
    this.data = data;
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad UsersListPage');
  }

}
