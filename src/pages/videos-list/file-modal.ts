import { Component } from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';
import {DomSanitizer} from "@angular/platform-browser";

/*
  Generated class for the FileModal page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-file-modal',
  templateUrl: 'file-modal.html'
})
export class FileModalPage {
  data: any;
  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public navParams: NavParams, public sanitizer: DomSanitizer) {
    this.data = navParams.get('content');
    console.log(this.data);
  }

  getSafeUrl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  getVimeoUrl(url) {
    let returnVal;
    if (url) {
      returnVal = url.replace('https://vimeo.com/', 'https://player.vimeo.com/video/') + '?autoplay=1&color=000000';
      // returnVal = 'https://player.vimeo.com/video/' + id + '?autoplay=1&color=000000';
    } else {
      returnVal = '';
    }
    return this.sanitizer.bypassSecurityTrustResourceUrl(returnVal);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FileModalPage');
  }

}
