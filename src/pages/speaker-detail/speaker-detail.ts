import {Component, ViewChild, ChangeDetectorRef} from '@angular/core';
import {NavController, NavParams, Content, LoadingController, Slides} from 'ionic-angular';
import moment from 'moment';

import { HomePage } from '../home/home';
import { ProgramingPage } from '../programing/programing';
import {DomSanitizer} from "@angular/platform-browser";
import {FirebaseData} from "../../providers/firebase-data";
import {BaseData} from "../../providers/base-data";
import {ProgramDetailPage} from "../program-detail/program-detail";
import {AlertLoader} from "../../providers/alert-loader";

/*
  Generated class for the SpeakerDetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-speaker-detail',
  templateUrl: 'speaker-detail.html'
})
export class SpeakerDetailPage {
  @ViewChild(Content) content: Content;
  @ViewChild(Slides) slides: Slides;
  data: any;
  speaker: any;
  isScroll : boolean;
  associateLectures: any;
  start_date: any;
  end_date: any;
  dis_start_date: any;
  dis_end_date: any;
  currentindex; any;
  currentDate: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private cref: ChangeDetectorRef,
    public sanitizer: DomSanitizer,
    public fb: FirebaseData,
    public bd: BaseData,
    public loadingCtrl: LoadingController,
    public alertLoader:AlertLoader
  ) {
    this.currentindex = 0;
    this.data = navParams.get('speaker');
    console.log(this.speaker);
    let eventId = this.navParams.get('eventkey');

    this.start_date = new Date().getTime();
    this.end_date = new Date().getTime() + 30*24*60*60*1000;
    this.dis_start_date = this.getValidDate(this.start_date);
    this.dis_end_date = this.getValidDate(this.end_date);


    let loading = this.loadingCtrl.create();
    loading.present();
    fb.list('lectures', {
      query: {
        orderByChild: 'event_id',
        equalTo: eventId
      }
    }).take(1).subscribe(lectures => {
      console.log(eventId, lectures);
      let tempdata = [];
      for (let i = 0; i < lectures.length; i++) {
        let speakers = lectures[i].speakers || [];
        for(let j = 0; j < speakers.length; j++){
          let item = speakers[j];
          if (item === this.data.$key) {
            tempdata.push(lectures[i]);
          }
        }
      }
      loading.dismiss();
      tempdata.sort((a: any, b: any) => {
        if (a.startTime < b.startTime) {
          return -1;
        } else if (a.startTime > b.startTime) {
          return 1;
        } else {
          return 0;
        }
      });
      this.associateLectures = this.changeDataStructure(tempdata);
      setTimeout(_ => {
        this.scrollChange();
      }, 500);
    });

    this.isScroll = true;
  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  changeDataStructure(data) {
    let tempArr = {};
    for (let i = 0; i < data.length; i++) {
      let lectureItem = data[i];
      let startDate = moment(lectureItem.startTime).format('YYYY-MM-DD');
      if (!tempArr.hasOwnProperty(startDate)) {
        this.currentDate =  startDate;
        tempArr[startDate] = [];
      }
      tempArr[startDate].push(lectureItem);
    }
    console.log('tempArr',tempArr);
    return tempArr;
  }

  changeTimeRange() {
    this.start_date = moment(this.dis_start_date).valueOf();
    this.end_date = moment(this.dis_end_date).valueOf();
    if (this.start_date === this.end_date) {
      this.end_date = this.start_date + 24*60*60*1000 - 1;
      this.dis_end_date = this.getValidDate(this.end_date);
    }
    if (this.start_date > this.end_date) {
      this.alertLoader.complexAlert('Por favor defina a data final novamente.', ()=>{
        this.end_date = this.start_date + 24*60*60*1000 - 1;
        this.dis_end_date = this.getValidDate(this.end_date);
      });
    }
    console.log(this.start_date, this.end_date);
  }

  goToSlide(index, currentDate) {
    this.currentDate = currentDate;
    this.currentindex = index;
    this.slides.slideTo(index, 500);
  }
  slideChanged() {
    this.currentindex = this.slides.getActiveIndex();
    console.log("Current index is", this.currentindex);
  }

  checkDataWithTimeRange(lecture) {
    if (lecture.startTime > this.start_date && lecture.startTime < this.end_date) {
      return true;
    } else {
      return false;
    }
  }

  getDateRageString() {
    let startString = moment(this.dis_start_date).format('M/D');
    let endString = moment(this.dis_end_date).format('M/D');
    return startString + ' ~ ' + endString;
  }

  getDateString(date) {
    return moment(date).format('D/M/YY');
  }

  getTimeString(date) {
    return moment(date).format('hh:mm');
  }

  getValidTimeRange(key) {
    moment.locale('pt');
    return moment(key).format('LL');
  }
  getValidDate(key) {
    return moment(key).format('YYYY-MM-DD');
  }

  getSafeUrl(url) {
    return (url)?this.sanitizer.bypassSecurityTrustResourceUrl(url): './assets/images/blank_user.jpg';
  }

  goToLectureDetial(lecture) {
    console.log(lecture);
    this.navCtrl.push(ProgramDetailPage, {'key': lecture.$key});
  }

  goToPro () {
    this.navCtrl.push(ProgramingPage, {key: 'any'});
  }

  scrollTo(element:string) {
    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 500);
    this.scrollChange();
  }

  scrollChange () {
    let dementions = this.content.getContentDimensions();
    if(dementions.scrollTop > 350) {
      this.isScroll = true;
    } else {
      this.isScroll = false;
    }
    this.cref.detectChanges();
    setTimeout(_ => {
      this.content.resize();
    }, 120);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad UsersListPage');
  }

}
