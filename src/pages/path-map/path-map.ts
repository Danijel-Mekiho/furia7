import {Component, ViewChild, ElementRef, ChangeDetectorRef} from '@angular/core';
import {NavController, NavParams, ViewController, LoadingController} from 'ionic-angular';

import {DomSanitizer} from "@angular/platform-browser";
import {FirebaseData} from "../../providers/firebase-data";
import {FirebaseAuth} from "../../providers/firebase-auth";

declare var google;
/*
 Generated class for the Promotion page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-path-map',
  templateUrl: 'path-map.html'
})
export class PathMapPage {
  @ViewChild('map') mapElement: ElementRef;
  map: any;

  eventLocation: any;
  myLocation:any;

  constructor(public navCtrl: NavController,
              public viewCtrl: ViewController,
              public navParams: NavParams,
              public sanitizer: DomSanitizer,
              public fb: FirebaseData,
              public loadingCtrl: LoadingController,
              public authFire: FirebaseAuth,
              ) {
    this.eventLocation = navParams.get("eventLocation");
    this.myLocation = navParams.get("myLocation");

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FileModalPage');

    /**
     *Event Location
     */
    let endLng = new google.maps.LatLng(this.eventLocation.lat, this.eventLocation.lng);
    console.log(this.eventLocation.lat, this.eventLocation.lng);

    /**
     * My Location
     */
    let startLng = new google.maps.LatLng(this.myLocation.lat, this.myLocation.lng);
    console.log(this.myLocation.lat, this.myLocation.lng)

    /**
     * My Location
     */
    let center = new google.maps.LatLng((this.eventLocation.lat*1+this.myLocation.lat*1)/2, (this.eventLocation.lng*1+this.myLocation.lng*1)/2);
    console.log((this.eventLocation.lat*1+this.myLocation.lat*1)/2, (this.eventLocation.lng*1+this.myLocation.lng*1)/2);


    console.log(endLng, startLng, center);
    let mapOptions = {
      center: center,
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);


    let directionsService = new google.maps.DirectionsService();



    let request = {
      origin: startLng,
      destination: endLng,
      travelMode: google.maps.TravelMode.DRIVING
    };
    directionsService.route(request, (response, status) => {
      console.log(response, status)
      if (status == google.maps.DirectionsStatus.OK) {
        let directionsDisplay:any = new google.maps.DirectionsRenderer();
        directionsDisplay.setDirections(response);
        directionsDisplay.setMap(this.map);
      } else {
        let bounds = new google.maps.LatLngBounds();
        let marker1 = new google.maps.Marker({
          map: this.map,
          position: startLng,
          visible: true
        });
        bounds.extend(marker1.getPosition());

        let marker2 = new google.maps.Marker({
          map: this.map,
          position: endLng,
          visible: true
        });
        bounds.extend(marker2.getPosition());
        this.map.fitBounds(bounds);
        //alert("Directions Request from " + start.toUrlValue(6) + " to " + endLng.toUrlValue(6) + " failed: " + status);
      }
    });

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }



}
