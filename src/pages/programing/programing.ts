import {Component, ViewChild, ChangeDetectorRef} from '@angular/core';
import {
  NavController, NavParams, Content, AlertController, LoadingController, Slides,
  PopoverController
} from 'ionic-angular';
import moment from 'moment';

import { HomePage } from '../home/home';
import {EventEnquetPage} from '../event-enquet/event-enquet';
import {FirebaseData} from "../../providers/firebase-data";
import {BaseData} from "../../providers/base-data";
import {DomSanitizer} from "@angular/platform-browser";
import {FirebaseAuth} from "../../providers/firebase-auth";
import {SpeakerDetailPage} from "../speaker-detail/speaker-detail";
import {AddPopoverPage} from "./add-popover";
import {Session} from "../../providers/session";
import {AlertLoader} from "../../providers/alert-loader";

/*
  Generated class for the Programing page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-programing',
  templateUrl: 'programing.html'
})
export class ProgramingPage {
  @ViewChild(Content) content: Content;
  @ViewChild(Slides) slides: Slides;
  isScroll : boolean;
  lectureData: any;
  collapseObj: any;
  simObj: any;
  data:any;
  userid: any;
  start_date: any;
  end_date: any;
  dis_start_date: any;
  dis_end_date: any;
  eventType: any;
  isLoading: any;
  currentindex; any;
  currentDate: any;
  isTooltipe: boolean;
  pageTitle : string;
  setedSim:any = [];
  questions:any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private cref: ChangeDetectorRef,
    public alertCtrl: AlertController,
    public fbauth: FirebaseAuth,
    public fb: FirebaseData,
    public bd: BaseData,
    public loadingCtrl: LoadingController,
    public sanitizer: DomSanitizer,
    public session: Session,
    public alertLoader:AlertLoader
  ) {
    this.currentindex = 0;
    this.isScroll = true;
    this.collapseObj = {};
    this.simObj = {};
    this.isLoading = true;
    this.start_date = new Date().getTime();
    this.end_date = new Date().getTime() + 30*24*60*60*1000;
    this.dis_start_date = this.getValidDate(this.start_date);
    this.dis_end_date = this.getValidDate(this.end_date);
    let eventId = this.navParams.get('key');
    this.eventType = this.navParams.get('type');
    this.userid = session.getUserId();
    console.log(this.eventType);
    if (this.eventType === 'event') {
      this.pageTitle = 'EVENTOS ALLERGAN';
    } else {
      this.pageTitle = 'ATIVIDADES CIENTÍFICAS';
    }
    let loading = this.loadingCtrl.create();
    loading.present();
    this.lectureData = fb.list('lectures', {
      query: {
        orderByChild: 'event_id',
        equalTo: eventId
      }
    });
    this.lectureData.take(1).subscribe(lectures => {
      console.log(eventId, lectures);
      let tempdata = lectures;
      for (let i = 0; i < lectures.length; i++) {
        let item = lectures[i];
        this.collapseObj[item.$key] = false;
        this.simObj[item.$key] = false;
        for (let key in item) {
          switch(key) {
            case 'speakers':
            case 'polls':
            case 'materials':
            case 'users':
            case 'vouchers': {
              if(item[key]) tempdata[i][key] = this.bd.getObjectFromKey(key, item[key]);
              break;
            }
            case 'registered_doctor': {
              for (let k = 0; k < item[key].length; k++) {
                let _regDoc = item[key][k];
                if (this.userid === _regDoc) {
                  this.simObj[item.$key] = true;
                  break;
                }
              }
              break;
            }
            default: {
              //statements;
              break;
            }
          }
        }
      }
      tempdata.sort((a: any, b: any) => {
        if (a.startTime < b.startTime) {
          return -1;
        } else if (a.startTime > b.startTime) {
          return 1;
        } else {
          return 0;
        }
      });
      this.data = this.changeDataStructure(tempdata);

      console.log(this.data);

      if (tempdata.length) {
        let isfirst = session.getFirst();
        if (!isfirst) {
          setTimeout(_ => {
            this.isTooltipe = true;
            setTimeout(_ => {
              this.hideTooltip();
            }, 3000);
          }, 1000);
        }
      }
      // this.data = tempdata;

      /**
       *get User Questions
       */
      fb.list('lecture_questions', {
        query: {
          orderByChild: 'user_key',
          equalTo: this.userid
        }
      }).take(1).subscribe((questions:any)=>{
        for(var i=0;i<questions.length;i++){
          let question = questions[i];
          for(var date in this.data){
            for(var j=0;j<this.data[date].length;j++){
              if(question.lecture_key == this.data[date][j].$key){
                if(!this.data[date][j].user_questions){
                  this.data[date][j].user_questions = [];
                }
                this.data[date][j].user_questions.push(question);
              }
            }
          }
        }
        console.log(this.data);
      })



      loading.dismiss();
      setTimeout(_ => {
        this.scrollChange();
      }, 500);
      console.log(this.simObj);
    });
  }

  hideTooltip() {
    this.isTooltipe = false;
    this.session.setFirst();
  }

  changeDataStructure(data) {
    let tempArr = {};
    for (let i = 0; i < data.length; i++) {
      let lectureItem = data[i];
      let startDate = moment(lectureItem.startTime).format('YYYY-MM-DD');
      if (!tempArr.hasOwnProperty(startDate)) {
        this.currentDate =  startDate;
        tempArr[startDate] = [];
      }
      tempArr[startDate].push(lectureItem);
    }
    console.log('tempArr',tempArr);
    return tempArr;
  }

  goToSlide(index, currentDate) {
    this.currentDate = currentDate;
    this.currentindex = index;
    this.slides.slideTo(index, 500);
    setTimeout(()=>{
      this.content.scrollToTop();
    })
  }
  slideChanged() {
    this.currentindex = this.slides.getActiveIndex();
    console.log("Current index is", this.currentindex);
  }

  changeTimeRange() {
    this.start_date = moment(this.dis_start_date).valueOf();
    this.end_date = moment(this.dis_end_date).valueOf();
    if (this.start_date === this.end_date) {
      this.end_date = this.start_date + 24*60*60*1000 - 1;
      this.dis_end_date = this.getValidDate(this.end_date);
    }
    if (this.start_date > this.end_date) {
      this.alertLoader.complexAlert('Por favor defina a data final novamente.', ()=>{
        this.end_date = this.start_date + 24*60*60*1000 - 1;
        this.dis_end_date = this.getValidDate(this.end_date);
      });
    }
    console.log(this.start_date, this.end_date);
  }

  checkDataWithTimeRange(lecture) {
    if (lecture.startTime > this.start_date && lecture.startTime < this.end_date) {
      return true;
    } else {
      return false;
    }
  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  getDateString(date) {
    return moment(date).format('DD/MM/YY');
  }

  getTimeString(date) {
    return moment(date).format('H:mm');
  }

  getValidTimeRange(key) {
    moment.locale('pt');
    return moment(key).format('LL');
  }
  getValidDate(key) {
    return moment(key).format('YYYY-MM-DD');
  }

  getEndTimeString(date, duration){
    return moment(date).add(duration, "minutes").format('H:mm');
  }

  getSafeUrl(url) {
    return (url)?this.sanitizer.bypassSecurityTrustResourceUrl(url): './assets/images/blank_user.jpg';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductPage');
  }

  scrollTo(element:string) {
    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 500);
    this.scrollChange();
  }

  scrollChange () {
    this.isLoading = false;
    let dementions = this.content.getContentDimensions();
    if(dementions.scrollTop > 100) {
      this.isScroll = true;
    } else {
      this.isScroll = false;
    }
    this.cref.detectChanges();
    setTimeout(_ => {
      this.content.resize();
    }, 120);
  }

  gotoUsersList(speaker, key) {
    this.navCtrl.push(SpeakerDetailPage, {speaker: speaker, eventkey: key});
  }

  gotoEnquet (key, poll) {
    this.navCtrl.push(EventEnquetPage, {'lectureId': key, 'poll': poll});
  }

  goToDownload (material) {
    console.log(material);
    window.open(material.file, '_blank');
  }

  toggleContent (key) {
    console.log(key, this.collapseObj);
    this.collapseObj[key] = !this.collapseObj[key];
  }

  setSim(lecture) {
    console.log('lecture', lecture);
    if (!this.simObj[lecture.$key]) {
      let confirm = this.alertCtrl.create({
        title: 'CONFIRME',
        message: 'As inscrições para esse evento é limitado, por favor tenha certeza que comparecerá para não tirar uma vaga de alguma pessoa que realmente irá participar.',
        mode:'ios',
        buttons: [
          {
            text: 'SIM',
            handler: () => {
              let serverData = this.fb.object(`lectures/${lecture.$key}/registered_doctor`);
              let sendData = lecture;
              let regDoctors = [];
              if (lecture.registered_doctor && lecture.registered_doctor.length) {
                regDoctors = lecture.registered_doctor;
              }


              regDoctors.push(this.userid);
              if (regDoctors.length > lecture.available_space) {
                let alert = this.alertCtrl.create({
                  title: 'CONFIRME',
                  subTitle: 'O registro para essa palestra está lotada. Infelizmente todas as vagas foram preenchidas.',
                  mode:'ios',
                  buttons: [{
                    text: 'SIM',
                    handler: () => {}
                  }]
                });
                alert.present();
              } else {
                sendData.registered_doctor = regDoctors;
                console.log(regDoctors);
                serverData.set(regDoctors).then(firebaseNewData => {
                  console.log('firebaseNewData', firebaseNewData);
                  this.simObj[lecture.$key] = true;
                  this.setedSim[lecture.$key] = true;
                }, error => {
                  console.log('firebaseNewerror', error);
                  this.simObj[lecture.$key] = false;
                  this.setedSim[lecture.$key] = false;
                });
              }
            }
          },
          {
            text: 'NÃO',
            handler: () => {
              console.log('Agree clicked');
            }
          }
        ]
      });
      confirm.present();
    } else {
      let confirm = this.alertCtrl.create({
        title: 'CONFIRME',
        message: 'Você confirma que não poderá comparecer ao evento.',
        mode:'ios',
        buttons: [
          {
            text: 'SIM',
            handler: () => {
              let serverData = this.fb.object(`lectures/${lecture.$key}/registered_doctor`);
              let sendData = lecture;
              let regDoctors = [];
              if (lecture.registered_doctor && lecture.registered_doctor.length) {
                regDoctors = lecture.registered_doctor;
                console.log('registered_doctor', lecture.registered_doctor);
                for (let i = 0; i < regDoctors.length; i++) {
                  let item = regDoctors[i];
                  if (item === this.userid){
                    regDoctors.splice(i, 1);
                    break;
                  }
                }
                sendData.registered_doctor = regDoctors;
                console.log(regDoctors);
                serverData.set(regDoctors).then(firebaseNewData => {
                  console.log('firebaseNewData', firebaseNewData);
                  this.simObj[lecture.$key] = false;
                  this.setedSim[lecture.$key] = false;
                }, error => {
                  console.log('firebaseNewerror', error);
                });
              }
            }
          },
          {
            text: 'NÃO',
            handler: () => {
              console.log('Agree clicked');
            }
          }
        ]
      });
      confirm.present();
    }

  }

  createNewQuestion(lecture, question){
    this.isLoading = true;

    let newItem = {
      created_at:(new Date()).valueOf(),
      lecture_key:lecture.$key,
      content:question,
      user_key:this.userid
    }
    let loading = this.loadingCtrl.create();
    loading.present();
    this.fb.push(`lecture_questions`, newItem).subscribe(()=>{
      this.questions[lecture.$key] = "";
      if(!lecture.user_questions){
        lecture.user_questions = [];
      }
      lecture.user_questions.push(newItem);
      loading.dismiss();
    })

  }

}
