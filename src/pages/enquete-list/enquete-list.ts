import {Component, ViewChild, ChangeDetectorRef} from '@angular/core';
import {NavController, NavParams, Content} from 'ionic-angular';
import moment from 'moment';

import {EventEnquetPage} from '../event-enquet/event-enquet';
import {FirebaseData} from "../../providers/firebase-data";
import { HomePage } from '../home/home';

/*
  Generated class for the EnqueteList page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-enquete-list',
  templateUrl: 'enquete-list.html'
})
export class EnqueteListPage {
  @ViewChild(Content) content: Content;
  isScroll : boolean;
  data: any;
  currentIndex: number;
  tempData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private cref: ChangeDetectorRef, public fb: FirebaseData) {
    this.isScroll = true;

    this.fb.list('enquetes').subscribe(snapshot => {
      this.data = this.groupBy(snapshot);
      console.log(this.data);
      setTimeout(_ => {
        this.scrollChange();
      }, 500);
    });

    this.currentIndex = 0;
    setTimeout(_ => {
      this.scrollChange();
    }, 500);
  }

  groupBy (data) {
    let groupObj = {};
    for(let i = 0; i < data.length; i++) {
      let item = data[i];
      if (!groupObj.hasOwnProperty(item.lecture_id)) {
        groupObj[item.lecture_id] = {title:item.title, avaliable_time: item.avaliable_time, content:[]};
      }
      groupObj[item.lecture_id].content.push(item);
    }
    return groupObj;
  }

  getValidTimeRange(key) {
    return moment(key).format('D/MM/YY HH:mm');
  }

  gotoEnquet (key, index) {
    this.navCtrl.push(EventEnquetPage, {key: key, index: index});
  }

  scrollTo(element:string) {
    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 500);
    this.scrollChange();
  }

  scrollChange () {
    let dementions = this.content.getContentDimensions();
    if(dementions.scrollTop > 150) {
      this.isScroll = true;
    } else {
      this.isScroll = false;
    }
    this.cref.detectChanges();
    setTimeout(_ => {
      this.content.resize();
    }, 120);
  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UsersListPage');
  }

}
