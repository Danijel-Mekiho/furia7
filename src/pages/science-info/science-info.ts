import {Component, ViewChild, ElementRef, ChangeDetectorRef} from '@angular/core';
import {NavController, ViewController, ModalController, Content, NavParams} from 'ionic-angular';
import { Geolocation } from 'ionic-native';
import moment from 'moment';
import {DomSanitizer} from "@angular/platform-browser";
declare var google;

import { HomePage } from '../home/home';
import { PathMapPage } from '../path-map/path-map';
/*
  Generated class for the ScienceInfo page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-science-info',
  templateUrl: 'science-info.html'
})
export class ScienceInfoPage {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild(Content) content: Content;
  map: any;
  data: any;
  user: any;
  isScroll : boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public modalCtrl: ModalController, private cref: ChangeDetectorRef, private sanitizer: DomSanitizer,) {
    this.data = this.navParams.get('item');
    this.isScroll = true;
    setTimeout(_ => {
      this.scrollChange();
    }, 500);
  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  getSafeUrl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  getValidTimeRange(key) {
    moment.locale('pt');
    return moment(key).format('LL');
  }

  ionViewDidEnter(){
    this.loadMap();
  }

  scrollTo(element:string) {
    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 500);
    this.scrollChange();
  }

  scrollChange () {
    let dementions = this.content.getContentDimensions();
    if(dementions.scrollTop > 100) {
      this.isScroll = true;
    } else {
      this.isScroll = false;
    }
    this.cref.detectChanges();
    setTimeout(_ => {
      this.content.resize();
    }, 120);
  }

  loadMap(){
    if(!this.data.location || (!this.data.location.lat && this.data.location.lng)){
      return true;
    }
    let lat = this.data.location.lat || 0;
    let lng = this.data.location.lng || 0;
    let latLng = new google.maps.LatLng(lat, lng);
    let mapOptions = {
      center: latLng,
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    let marker = new google.maps.Marker({
      map: this.map,
      position: latLng,
      visible: true
    });
  }

  showpathModal(data:any){
    Geolocation.getCurrentPosition({enableHighAccuracy:true}).then(res => {
      let myLocation = {lat:res.coords.latitude, lng:res.coords.longitude};
      let modal = this.modalCtrl.create(PathMapPage, {eventLocation:this.data.location, myLocation:myLocation});
      modal.onDidDismiss(data => {
        console.log(data);
      });
      modal.present();
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
