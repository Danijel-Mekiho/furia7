import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import { EventosPage } from '../eventos/eventos';
import { SciencePage } from '../science/science';
import { VideosListPage } from '../videos-list/videos-list';
import {BaseData} from "../../providers/base-data";
import {Session} from "../../providers/session";
import {FirebaseAuth} from "../../providers/firebase-auth";
import {FirebaseData} from "../../providers/firebase-data";
/*
 Generated class for the Home page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  loading: any;
  role: any;
  selectPage:any;
  userid: any;
  isEvent: boolean;
  isLoadContent: boolean;
  constructor(public nav: NavController, public navParams: NavParams, public baseData: BaseData, public session: Session, public fbauth: FirebaseAuth, public loadingCtrl: LoadingController, public fb: FirebaseData) {
    this.isLoadContent = true;
    let isLogin = this.navParams.get('isLogin');
    if (isLogin) {
      let current_user = session.getLogin();
      console.log(current_user);
      this.role = current_user.role || 'viewer';
    } else {
      this.role = 'viewer';
    }
    this.selectPage = {
      eventos: false,
      science: false,
      videos: false
    };
    this.isEvent = false;

    baseData.init();

    this.userid = session.getUserId();
    let loading = this.loadingCtrl.create();
    loading.present();
    fb.list('events', {
      query: {
        orderByChild: 'type',
        equalTo: 0
      }
    }).subscribe(snapshot => {
      if (snapshot.length) {
        for (let k = 0; k < snapshot.length; k++) {
          let item = snapshot[k];
          if(item.doctors) {
            for (let i = 0; i < item.doctors.length; i++) {
              let tItem = item.doctors[i];
              if (tItem === this.userid) {
                this.isEvent = true;
                break;
              }
            }
          }
        }

      }
      loading.dismiss();

      console.log(snapshot);
    });
    this.isLoadContent = true;
  }


  ionViewDidEnter() {
    if (!this.isLoadContent) {
      setTimeout(_ => {
        this.isLoadContent = true;
      }, 100);
    }
    console.log('Did load');
  }
  ionViewDidLeave () {
    this.isLoadContent = false;
    console.log('Did Leave');
  }

  gotoPage (page) {
    this.selectPage = {
      eventos: false,
      science: false,
      videos: false
    };
    this.selectPage[page] = true;
    console.log(page, this.selectPage);
    switch(page) {
      case 'eventos': {
        this.nav.push(EventosPage);
        break;
      }
      case 'science': {
        this.nav.push(SciencePage);
        break;
      }
      case 'videos': {
        this.nav.push(VideosListPage);
        break;
      }
      default: {
        //statements;
        break;
      }
    }
  }

  goToFeed(category: any) {

  }
}
