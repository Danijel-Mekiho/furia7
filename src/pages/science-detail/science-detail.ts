import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {DomSanitizer} from "@angular/platform-browser";
import moment from 'moment';

import { HomePage } from '../home/home';
import { ScienceInfoPage } from '../science-info/science-info';
import { ProgramingPage } from '../programing/programing';
import { UsersListPage } from '../users-list/users-list';
/*
  Generated class for the ScienceDetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-science-detail',
  templateUrl: 'science-detail.html'
})
export class ScienceDetailPage {
  data:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private sanitizer: DomSanitizer) {
    this.data = this.navParams.get('item');
    console.log(this.data);
  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  getSafeUrl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  getValidTimeRange(key) {
    moment.locale('pt');
    return moment(key).format('LL');
  }
  goToInfo() {
    this.navCtrl.push(ScienceInfoPage, {item: this.data});
  }
  goToPro() {
    this.navCtrl.push(ProgramingPage, {key: this.data.$key, type: 'congress'});
  }

  goToUsersList() {
    this.navCtrl.push(UsersListPage, {key: this.data.$key});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScienceDetailPage');
  }

}
