import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {DomSanitizer} from '@angular/platform-browser';
import moment from 'moment';
import _ from 'lodash';

import { HomePage } from '../home/home';
import { ScienceDetailPage } from '../science-detail/science-detail';
import {FirebaseData} from "../../providers/firebase-data";
/*
  Generated class for the Science page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-science',
  templateUrl: 'science.html'
})
export class SciencePage {
  congressesData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private fb: FirebaseData, public loadingCtrl: LoadingController, private sanitizer: DomSanitizer) {
    let loading = this.loadingCtrl.create();
    loading.present();
    fb.list('events', {
      query: {
        orderByChild: 'type',
        equalTo: 1
      }
    }).subscribe(snapshot => {
      let currentTimestamp = (new Date()).valueOf();
      let congressesArray = _.filter(snapshot, (item:any) => {
        if(moment(currentTimestamp).diff(item.end_time, 'days') > 30){
          return false;
        }else{
          return true;
        }
      })
      this.congressesData = _.orderBy(congressesArray, ['start_time'], ['asc']);
      loading.dismiss();
      console.log(snapshot);
    });
  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  getSafeUrl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  goToDetail (item) {
    this.navCtrl.push(ScienceDetailPage, {item: item})
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SciencePage');
  }

}
