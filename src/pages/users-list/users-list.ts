import {Component, ViewChild, ChangeDetectorRef} from '@angular/core';
import {NavController, NavParams, Content, LoadingController} from 'ionic-angular';
import _ from 'lodash';

import { ProgramingPage } from '../programing/programing';
import {FirebaseData} from "../../providers/firebase-data";
import {FirebaseAuth} from "../../providers/firebase-auth";
import {BaseData} from "../../providers/base-data";
import {SpeakerDetailPage} from "../speaker-detail/speaker-detail";
import {DomSanitizer} from "@angular/platform-browser";
import { HomePage } from '../home/home';

/*
  Generated class for the UsersList page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-users-list',
  templateUrl: 'users-list.html'
})
export class UsersListPage {
  @ViewChild(Content) content: Content;
  isScroll : boolean;
  lectureData: any;
  data: any;
  eventId: any;
  isLoading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private cref: ChangeDetectorRef,
    public loadingCtrl: LoadingController,
    public sanitizer: DomSanitizer,
    public fb: FirebaseData,
    public fbauth: FirebaseAuth,
    public bd: BaseData
  ) {
    this.isScroll = true;
    this.data = [];
    this.isLoading = true;
    this.eventId = this.navParams.get('key');
    let loading = this.loadingCtrl.create();
    loading.present();
    // Get Lectures from event id
    this.lectureData = fb.list('lectures', {
      query: {
        orderByChild: 'event_id',
        equalTo: this.eventId
      }
    });
    this.lectureData.take(1).subscribe(lectures => {
      console.log(lectures);
      let tempdata = lectures;
      for (let i = 0; i < lectures.length; i++) {
        let item = lectures[i];
        if(item.speakers && item.speakers.length) {
          // Get full Speakers list from speaker id array
          let speakerTemp = this.bd.getObjectFromKey('speakers', item.speakers);

          for (let ind = 0; ind < speakerTemp.length; ind++) {
            let tempItem = speakerTemp[ind];
            let isExistSpeaker = false;
            // Check duplicated speakers.
            for (let ind2 = 0; ind2 < this.data.length; ind2++) {
              let tempItem2 = this.data[ind2];
              if (tempItem.$key === tempItem2.$key) {
                isExistSpeaker = true;
                break;
              }
            }
            if (!isExistSpeaker) this.data.push(tempItem);
          }
          this.data = _.orderBy(this.data,['name'],['asc']);
          console.log('speakers', this.data);
        }
      }

      loading.dismiss();
      setTimeout(_ => {
        this.scrollChange();
      }, 500);
    });

  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  getSafeUrl(url) {
    return (url)?this.sanitizer.bypassSecurityTrustResourceUrl(url): './assets/images/blank_user.jpg';
  }

  goToPro () {
    this.navCtrl.push(ProgramingPage, {key: 'any'});
  }

  goToSpeakerDetial(speaker){
    this.navCtrl.push(SpeakerDetailPage, {speaker: speaker, eventkey: this.eventId});
  }

  scrollTo(element:string) {
    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 500);
    this.scrollChange();
  }

  scrollChange () {
    this.isLoading = false;
    let dementions = this.content.getContentDimensions();
    if(dementions.scrollTop > 150) {
      this.isScroll = true;
    } else {
      this.isScroll = false;
    }
    this.cref.detectChanges();
    setTimeout(_ => {
      this.content.resize();
    }, 120);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad UsersListPage');
  }

}
