import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {FirebaseData} from "../../providers/firebase-data";

/*
  Generated class for the AdminTest page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-admin-test',
  templateUrl: 'admin-test.html'
})
export class AdminTestPage {
  LectureData: any;
  SpeakerData: any;
  PollData: any;
  EventsData: any;
  CongressData: any;
  MaterialData: any;
  TravelData: any;
  PromotionData: any;
  VoucherData: any;
  data: any;

  currentData:string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public fb: FirebaseData) {
    this.currentData = 'lectures';
    this.data = [];
    this.LectureData = this.fb.list('lectures');
    this.SpeakerData = this.fb.list('speakers');
    this.PollData = this.fb.list('polls');
    this.EventsData = this.fb.list('events');
    this.CongressData = this.fb.list('congresses');
    this.MaterialData = this.fb.list('materials');
    this.TravelData = this.fb.list('travels');
    this.PromotionData = this.fb.list('promotions');
    this.VoucherData = this.fb.list('vouchers');
  }

  checkData(dataname) {
    this.currentData = dataname;
    this.data = [];
    switch(dataname) {
      case 'lectures': {
        this.LectureData.subscribe(snapshot => {
          this.data = snapshot;
        });
        break;
      }
      case 'speakers': {
        this.SpeakerData.subscribe(snapshot => {
          this.data = snapshot;
        });
        break;
      }
      case 'polls': {
        this.PollData.subscribe(snapshot => {
          this.data = snapshot;
        });
        break;
      }
      case 'events': {
        this.EventsData.subscribe(snapshot => {
          this.data = snapshot;
        });
        break;
      }
      case 'congresses': {
        this.CongressData.subscribe(snapshot => {
          this.data = snapshot;
        });
        break;
      }
      case 'materials': {
        this.MaterialData.subscribe(snapshot => {
          this.data = snapshot;
        });
        break;
      }
      case 'travels': {
        this.TravelData.subscribe(snapshot => {
          this.data = snapshot;
        });
        break;
      }
      case 'promotions': {
        this.PromotionData.subscribe(snapshot => {
          this.data = snapshot;
        });
        break;
      }
      case 'vouchers': {
        this.VoucherData.subscribe(snapshot => {
          this.data = snapshot;
        });
        break;
      }
      default: {
        this.data = [];
        //statements;
        break;
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminTestPage');
  }

  addNewData(dataname) {

    switch(dataname) {
      case 'lectures': {
        let sendData = {
          name: 'Cafe da manha',
          desc: 'Irony chillwave small batch hoodie butcher viral. Chambray sustainable tempor, eu letterpress adipisicing lo-fi fashion axe nesciunt roof party laborum church-key artisan umami sint.',
          date: new Date().getTime(),
          startTime: new Date().getTime(),
          duration: 30,
          event_id: '-KgxODkLxEgkdBtxWj6x',
          speakers: [
            '-KgxDC6Go1ER4WhfQm0p', '-KgxAKsud62JHIYYiNl4'
          ],
          materials: [
            '-KgxD8hfOed-wjTGKDhx', '-KgxD689xcJepfq-xATk'
          ],
          polls: [
            '-KgxD9c1FZwd0FwzOKKo', '-KgxD9gHQXbzAn2-LGHv', '-KgxD9nmTd8hooXUzzpz'
          ],
          registered_doctor: [
            '5EDTJ0HgVMXajDdRkFs4V7KLNJZ2'
          ],
          available_space: 10,
        };
        this.LectureData.push(sendData).then(newData => {
          console.log(newData);
        }, error => {
          console.log(error);
        });
        break;
      }
      case 'speakers': {
        let sendData = {
          name: 'Test speakers 1',
          photo: 'url',
          desc: '08:00'
        };
        this.SpeakerData.push(sendData).then(newData => {
          console.log(newData);
        }, error => {
          console.log(error);
        });
        break;
      }
      case 'polls': {

        let sendData = {
          available_time: new Date().getTime(),
          questions:[
            {
              ischeck: false,
              question: 'Irony chiing lo-fi fashion axe nesciunt roof party laborum church-key artisan umami sint.',
              answer: false
            },
            {
              ischeck: false,
              question: 'Irony chillwave small batch hooicing lo-fi fashion axe nesciunt roof party laborum church-key artisan umami sint.Irony chillwave small batch hoodie butcher viral. Chambray sustainable tempor, eu letterpress adipisicing lo-fi fashion axe nesciunt roof party laborum church-key artisan umami sint.',
              answer: false
            },
            {
              ischeck: false,
              question: 'Irony chillwave small batch hoodie butcher viral. Chambray sustainable tempor, eu letterpress adipisicing lo-fi fashion axe nesciunt roof party laborum church-key artisan umami sint.',
              answer: false
            }
          ],
          lecture_id: 'lecture_id',
          image: 'image link'
        };
        this.PollData.push(sendData).then(newData => {
          console.log(newData);
        }, error => {
          console.log(error);
        });
        break;
      }
      case 'events': {
        let sendData = {
          name: 'Event 1',
          logo: 'https://firebasestorage.googleapis.com/v0/b/allergan-d3871.appspot.com/o/images%2Fsim-logo.png?alt=media&token=613bff79-d8cf-49a6-99b0-a249dcab1f70',
          desc: 'Delectus gluten-free beard meh, forage flannel tofu nisi accusamus cupidatat commodo before they sold out. Irony chillwave small batch hoodie butcher viral. Chambray sustainable tempor, eu letterpress adipisicing lo-fi fashion axe nesciunt roof party laborum church-key artisan umami sint. Irony chillwave small batch hoodie butcher viral. Chambray sustainable tempor, eu letterpress adipisicing lo-fi fashion axe nesciunt roof party laborum church-key artisan umami sint.',
          location: {
            address: 'São José do rio preto',
            lat: -23.539548,
            lng: -46.596838
          },
          picture: 'https://firebasestorage.googleapis.com/v0/b/allergan-d3871.appspot.com/o/images%2F500x333Waterpolo.png?alt=media&token=6529dc9d-75c0-47f7-a300-2368daca0298',
          start_time: new Date().getTime(),
          end_time: new Date().getTime(),
          type: 0,
          status: 0,
          contact_info: {
            site: 'www.google.com',
            email: 'viktorbelashov@yahoo.com',
            telephone: '(12)123456789'
          }
        };
        this.EventsData.push(sendData).then(newData => {
          console.log(newData);
        }, error => {
          console.log(error);
        });
        break;
      }
      case 'congresses': {
        let sendData = {
          name: 'Congress 1',
          logo: 'logo url',
          desc: 'Delectus gluten-free beard meh, forage flannel tofu nisi accusamus cupidatat commodo before they sold out.',
          location: 'This is test location.',
          picture: 'picture url',
          start_time: new Date().getTime(),
          end_time: new Date().getTime(),
          type: 1,
          status: 0,
          contact_info: 'Delectus gluten-free beard meh, forage flannel tofu nisi accusamus cupidatat commodo before they sold out.'
        };
        this.CongressData.push(sendData).then(newData => {
          console.log(newData);
        }, error => {
          console.log(error);
        });
        break;
      }
      case 'materials': {
        let sendData = {
          type: 'pdf',
          private: 'logo url',
          available_time: new Date().getTime(),
          file: 'file link',
          lecture_id: 'lecture_id',
          voucher_id: 'voucher_id'
        };
        this.MaterialData.push(sendData).then(newData => {
          console.log(newData);
        }, error => {
          console.log(error);
        });
        break;
      }
      case 'travels': {
        let sendData = {
          hotel_info: 'Hotel Information.',
          agency_info: 'Agency Information.',
          airticket_info: 'Air Ticket Information.',
          trasfer_info: 'Transfer Information',
          event_id: 'event_id'
        };
        this.TravelData.push(sendData).then(newData => {
          console.log(newData);
        }, error => {
          console.log(error);
        });
        break;
      }
      case 'promotions': {
        let sendData = {
          image: 'Image link.',
          start_time: new Date().getTime(),
          end_time: new Date().getTime(),
          voucher_id: 'voucher_id',
          link: 'link'
        };
        this.PromotionData.push(sendData).then(newData => {
          console.log(newData);
        }, error => {
          console.log(error);
        });
        break;
      }
      case 'vouchers': {
        let sendData = {
          voucher: 'Voucher.',
          start_time: new Date().getTime(),
          end_time: new Date().getTime()
        };
        this.VoucherData.push(sendData).then(newData => {
          console.log(newData);
        }, error => {
          console.log(error);
        });
        break;
      }
      default: {
        //statements;
        break;
      }
    }


  }

}
