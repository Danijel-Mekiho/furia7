import {Component} from '@angular/core';
import {NavController, LoadingController, ActionSheetController} from 'ionic-angular';
import {Validators, FormGroup, FormControl} from '@angular/forms';
import _ from 'lodash';

import {Camera} from 'ionic-native';

import {HomePage} from '../home/home';
import {LoginPage} from '../login/login';
import {TermsOfServicePage} from '../terms-of-service/terms-of-service';
import {FirebaseAuth} from "../../providers/firebase-auth";
import {FirebaseStorage} from "../../providers/firebase-storage";
import {AngularFire} from "angularfire2";
import {Tools} from "../../providers/tools";
import {Session} from "../../providers/session";
import {Utilities} from "../../providers/utilities";

@Component({
  selector: 'signup-page',
  templateUrl: 'signup.html'
})
export class SignupPage {
  signup: FormGroup;
  main_page: { component: any };
  error: any;
  dataAvatar: any;
  image: any;
  ufData: any;
  cpfValid:boolean = true;



  constructor(
    public nav: NavController,
    public loadingCtrl: LoadingController,
    public af: AngularFire,
    public auth: FirebaseAuth,
    public fbStorage: FirebaseStorage,
    public tools: Tools,
    public session: Session,
    public utilities: Utilities,
    public actionSheet: ActionSheetController
  ) {
    this.main_page = {component: HomePage};

    tools.getUFData().subscribe(data => {
      this.ufData = data;
    });


    this.signup = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.pattern('^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$')]),
      crm: new FormControl('', Validators.required),
      uf: new FormControl('', Validators.required),
      speciality: new FormControl('', Validators.required),
      cpf: new FormControl('', Validators.required),
      city: new FormControl('', Validators.required),
      gender: new FormControl('male', Validators.required),
      phonenumber: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      confirm_password: new FormControl('', Validators.required),
      confirm_terms_of_service: new FormControl(false)
    });
  }

  takePicture(type) {
    let options = {
      quality: 30,
      targetWidth: 720,
      sourceType: (type==='camera')?Camera.PictureSourceType.CAMERA:Camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: Camera.DestinationType.FILE_URI,
      allowEdit: true,
      correctOrientation: true,
      encodingType: Camera.EncodingType.JPEG,
      // popoverOptions: new CameraPopoverOptions(300, 300, 100, 100, navigator.camera.PopoverArrowDirection.ARROW_ANY),
      saveToPhotoAlbum: false
    };
    Camera.getPicture(options).then((imageData) => {
      // let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.image = imageData;
      this.dataAvatar = imageData;
    }, (err) => {
      // Handle error
    });
  }

  onSubmit($event:any){
    console.log("submit clicked");
    this.doSignUp();
  }

  doSignUp() {
    this.error = '';
    let signUpItem = this.signup.value;
    console.log("checkValud");
    console.log(this.signup.valid);
    console.log(signUpItem);
    console.log(this.signup)

    if(!this.signup.valid){
      this.error = 'Todos os campos são obrigatórios, por favor preencha com os seus dados';
      return true;
    }
    if (signUpItem.password != signUpItem.confirm_password) {
      this.error = 'A senha não confere, por favor insira novamente.';
      return true;
    }
    if(signUpItem.password && signUpItem.password.length < 6){
      this.error = 'A senha deve ter 6 caracteres ou mais.';
      return true;
    }

    if(!this.cpfValid){
      this.error ='Erro, Esse número de CPF não é válido';
      return true;
    }

    if(!signUpItem.confirm_terms_of_service){
      this.error = 'Os termos de uso é obrigatório, por favor clique no check box';
      return true;
    }

    let loading = this.loadingCtrl.create({
      content: 'Salvando...'
    });
    loading.present();

    let ufRow:any = _.find(this.ufData, {ID:signUpItem.uf});
    this.utilities.validCRM(signUpItem.crm, ufRow.Sigla).subscribe((res)=>{
      loading.dismiss();
      if(res.status){
        delete signUpItem.confirm_password;
        if (this.image) {
          let loading = this.loadingCtrl.create({
            content: 'Upload photo...'
          });
          loading.present();
          let adminData = {email: 'viktorbelashov824@gmail.com', password: 'Ejfehd2015'}
          this.auth.loginWithEmail(adminData).subscribe(loginData => {
            this.fbStorage.upload(this.image).then(data => {
              console.log('resulttttttttttttttt', data);
              let resData: any = data;
              signUpItem.photo = resData.downloadUrl;
              loading.dismiss();
              this.changeUserProfile();
            });
          }, error => {
            setTimeout(() => {
              loading.dismiss();
              this.error = error;
            }, 1000);
          });
        } else {
          signUpItem.photo = '';
          this.changeUserProfile();
        }

      }else{
        this.error ='Erro, esse número de CRM não é válido.';
        return true;
      }
    })
  }

  chooseRes() {
    let ac_sheet = this.actionSheet.create({
      buttons: [
        {
          text: 'Gallery',
          handler: () => {
            this.takePicture('gallery');
          }
        }, {
          text: 'Camera',
          handler: () => {
            this.takePicture('camera');
          }
        }
      ]
    });
    ac_sheet.present();
  }

  changeUserProfile () {
    let loading = this.loadingCtrl.create({
      content: 'Salvando...'
    });
    loading.present();
    let signUpItem = this.signup.value;
    delete signUpItem.confirm_password;
    console.log('signUpItem', signUpItem);
    this.auth.registerUser(signUpItem).subscribe(registerData => {
      let sessionData = {
        cpf: signUpItem.cpf,
        password: ''
      };
      this.session.setUser(sessionData);
      setTimeout(() => {
        loading.dismiss();
        this.nav.push(LoginPage);
      }, 1000);

    }, registerError => {
      console.log('registerError ', registerError);
      if(/^The\semail/.test(registerError)){
        registerError = "O e-mail informado não é válido";
      }
      if(/^Password\sshould/.test(registerError)){
        registerError = "A senha deve ter 6 caracteres ou mais";
      }
      console.log(registerError);
      setTimeout(() => {
        loading.dismiss();
        this.error = registerError;
      }, 1000);
    });
  }

  oncpfChange(){
    this.cpfValid = this.utilities.validaCPF(this.signup.value.cpf);
  }

  gotoTermsPage(event){
    this.nav.push(TermsOfServicePage, {hidehomebutton: true});
    event.preventDefault();
  }

  doCancel() {
    this.nav.pop();
  }

}
