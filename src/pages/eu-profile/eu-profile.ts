/*
  Generated class for the EuProfile page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
import { Component } from '@angular/core';
import {NavController, LoadingController} from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import _ from 'lodash';

import { Camera } from 'ionic-native';

import {FirebaseAuth} from "../../providers/firebase-auth";
import {FirebaseStorage} from "../../providers/firebase-storage";
import {AngularFire} from "angularfire2";
import {Tools} from "../../providers/tools";
import { HomePage } from '../home/home';
import {Session} from "../../providers/session";
import {Utilities} from "../../providers/utilities";

@Component({
  selector: 'page-eu-profile',
  templateUrl: 'eu-profile.html'
})
export class EuProfilePage {
  signup: FormGroup;
  main_page: { component: any };
  error: any;
  dataAvatar: any;
  image: any;
  userid: any;
  ufData: any;
  cpfValid:boolean = true;
  currentUser:any;

  constructor(
    public nav: NavController,
    public loadingCtrl: LoadingController,
    private af: AngularFire,
    public auth: FirebaseAuth,
    public fbStorage: FirebaseStorage,
    public tools: Tools,
    public session: Session,
    public utilities: Utilities,
  ) {
    this.main_page = { component: HomePage };
    this.userid = session.getUserId();
    auth.getUserData();
    tools.getUFData().subscribe(data => {
      this.ufData = data;
    });

    this.auth.getUserData().subscribe(userdata => {
      this.currentUser = userdata;
      console.log('userdata', userdata);
      this.signup = new FormGroup({
        name: new FormControl(userdata.name, Validators.required),
        email: new FormControl(userdata.email, Validators.required),
        uf: new FormControl(userdata.uf || '1', Validators.required),
        crm: new FormControl(userdata.crm, Validators.required),
        city: new FormControl(userdata.city, Validators.required),
        speciality: new FormControl(userdata.speciality, Validators.required),
        cpf: new FormControl(userdata.cpf, Validators.required),
        gender: new FormControl(userdata.gender, Validators.required),
        phonenumber: new FormControl(userdata.phonenumber, Validators.required)
      });
      this.dataAvatar = userdata.photo || '';
    }, error => {
      console.log('registerError', error);
      this.signup = new FormGroup({
        name: new FormControl('', Validators.required),
        email: new FormControl('', Validators.required),
        uf: new FormControl('1', Validators.required),
        crm: new FormControl('', Validators.required),
        city: new FormControl('', Validators.required),
        speciality: new FormControl('', Validators.required),
        cpf: new FormControl('', Validators.required),
        phonenumber: new FormControl('', Validators.required),
        password: new FormControl('', Validators.required),
        confirm_password: new FormControl('', Validators.required)
      });
    });


  }

  goHome () {
    this.nav.setRoot(HomePage)
  }

  takePicture() {
    let options = {
      quality: 30,
      targetWidth: 720,
      sourceType: Camera.PictureSourceType.CAMERA,
      destinationType: Camera.DestinationType.FILE_URI,
      allowEdit: true,
      correctOrientation: true,
      encodingType: Camera.EncodingType.JPEG,
      // popoverOptions: new CameraPopoverOptions(300, 300, 100, 100, navigator.camera.PopoverArrowDirection.ARROW_ANY),
      saveToPhotoAlbum: false
    };
    Camera.getPicture(options).then((imageData) => {
      this.image = imageData;
      this.dataAvatar = imageData;
    }, (err) => {
      // Handle error
    });
  }

  onSubmit($event:any){
    console.log("submit clicked");
    this.doChange();
  }

  doChange() {
    this.error = '';
    let signUpItem = this.signup.value;
    console.log(signUpItem);

    if(!this.signup.valid){
      this.error = 'Todos os campos são obrigatórios, por favor preencha com os seus dados';
      return true;
    }

    // if(!this.cpfValid){
    //   this.error ='Erro, Esse número de CPF não é válido';
    //   return true;
    // }

    if (signUpItem.password === signUpItem.confirm_password) {
      if (this.image) {
        let loading = this.loadingCtrl.create({
          content: 'Upload photo...'
        });
        loading.present();
        this.fbStorage.upload(this.image).then(data => {
          console.log('resulttttttttttttttt', data);
          let resData: any = data;
          signUpItem.photo = resData.downloadUrl;
          loading.dismiss();
          this.changeUserProfile(signUpItem);
        });
      } else {
        signUpItem.photo = this.dataAvatar;
        this.changeUserProfile(signUpItem);
      }
    } else {
      this.error = 'A senha não confere, por favor insira novamente.';
    }

  }

  oncpfChange(){
    this.cpfValid = this.utilities.validaCPF(this.signup.value.cpf);
  }

  doCancel() {
    this.nav.pop();
  }

  changeUserProfile (credentials) {
    let loading = this.loadingCtrl.create({
      content: 'Salvando...'
    });
    loading.present();

    let user:any= this.af.database.object('users/' + this.currentUser.$key);
    user.set(credentials).then(()=>{
      loading.dismiss();
    }, error=>{
      alert("error occoured");
      loading.dismiss();
    })
  }
}
