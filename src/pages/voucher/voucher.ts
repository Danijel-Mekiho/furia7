import { Component } from '@angular/core';
import {NavController, LoadingController} from 'ionic-angular';

import {FirebaseAuth} from "../../providers/firebase-auth";
import {AngularFire} from "angularfire2";
import {Tools} from "../../providers/tools";
import {Session} from "../../providers/session";
import {Utilities} from "../../providers/utilities";

import { HomePage } from '../home/home';
/*
  Generated class for the Voucher page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-voucher',
  templateUrl: 'voucher.html'
})
export class VoucherPage {
  voucher: any;
  error: any;
  userid: any;

  constructor(public nav: NavController, public loadingCtrl: LoadingController, public af: AngularFire, public authFire: FirebaseAuth, public tools: Tools, public session: Session) {
    this.voucher = '';
    this.error = {status: 0, message:''};
    this.userid = session.getUserId();
  }

  confirmVoucher(){
    this.error = {status: 0, message:''};
    if(this.voucher){
      let loading = this.loadingCtrl.create();
      loading.present();
      this.tools.confirmVoucher(this.voucher).subscribe(data => {
        let result: any;
        result = data;

        if (result._body === 'success') {
          let user_vouchers = this.af.database.object('users/' + this.userid);
          user_vouchers.take(1).subscribe(user => {
            console.log('vouchers', user);
            loading.dismiss();
            let isVoucherExist = false;
            if (user.vouchers) {
              for (let i = 0; i < user.vouchers.length; i++) {
                let voucherItem = user.vouchers[i];
                if (voucherItem === this.voucher) {
                  isVoucherExist = true;
                }
              }
            } else {
              user.vouchers = [];
            }

            this.session.setVoucher(this.voucher);
            if (!isVoucherExist) {
              user.vouchers.push(this.voucher);
              user_vouchers.set(user)
                .then(_ => console.log("Updated"))
                .catch(err => console.log(err, "Failed"));
              this.error.status = 1;
              this.error.message = 'Voucher inserido com sucesso. Clique em voltar e em seguida no botão Vídeos e Treinamentos para visualizar o arquivo.';
            } else {
              this.error.status = 1;
              this.error.message = 'Voucher inserido com sucesso. Clique em voltar e em seguida no botão Vídeos e Treinamentos para visualizar o arquivo.';
            }
          });
        } else if (result._body === 'nothing') {
          loading.dismiss();
          this.error.status = 0;
          this.error.message = 'Esse número de voucher não existe.';
        } else if (result._body === 'after') {
          loading.dismiss();
          this.error.status = 0;
          this.error.message = 'Esse número de voucher está expirado.';
        } else if (result._body === 'before') {
          loading.dismiss();
          this.error.status = 0;
          this.error.message = 'Este número do voucher não é valido, em breve entre-o novamente.';
        } else {
          loading.dismiss();
          this.error.status = 0;
          this.error.message = 'Sever error.';
        }
      });
    } else {
      this.error.status = 0;
      this.error.message = 'Por favor insira o número de voucher.';
    }
  }

  goHome () {
    this.nav.setRoot(HomePage)
  }

}
