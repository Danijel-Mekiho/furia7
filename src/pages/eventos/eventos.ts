import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController} from 'ionic-angular';
import {DomSanitizer} from '@angular/platform-browser';
import moment from 'moment';
import _ from 'lodash';

import { HomePage } from '../home/home';
import { EventDetailPage } from '../event-detail/event-detail';
import {FirebaseData} from "../../providers/firebase-data";
import {FirebaseAuth} from "../../providers/firebase-auth";
import {Session} from "../../providers/session";
/*
  Generated class for the Eventos page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-eventos',
  templateUrl: 'eventos.html'
})
export class EventosPage {
  eventsData: any;
  userid: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FirebaseData,
    public loadingCtrl: LoadingController,
    private sanitizer: DomSanitizer,
    public fbauth: FirebaseAuth,
    public session: Session
  ) {
    this.userid = session.getUserId();
    let loading = this.loadingCtrl.create();
    loading.present();
    fb.list('events', {
      query: {
        orderByChild: 'type',
        equalTo: 0
      }
    }).subscribe(snapshot => {
      let currentTimestamp = (new Date()).valueOf();
      let eventsArray = _.filter(snapshot, (item:any) => {
        if(moment(currentTimestamp).diff(item.end_time, 'days') > 30){
          return false;
        }else{
          return true;
        }
      })
      this.eventsData = _.orderBy(eventsArray, ['start_time'], ['asc']);

      loading.dismiss();
      console.log(snapshot);
    });
  }

  isMine(item) {
    let returnVal = false;
    if(item.doctors) {
      for (let i = 0; i < item.doctors.length; i++) {
        let tItem = item.doctors[i];
        if (tItem === this.userid) returnVal = true;
      }
    }
    return returnVal;
  }

  getSafeUrl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  getValidTimeRange(key) {
    return moment(key).format('LL');
  }

  goToDetail (item) {
    this.navCtrl.push(EventDetailPage, {item: item})
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventosPage');
  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

}
