import {NavController, LoadingController, Events} from 'ionic-angular';
import {Component} from '@angular/core';
import { Toast } from 'ionic-native';

import {FirebaseAuth} from '../../providers/firebase-auth';

import {Session} from "../../providers/session";
import { HomePage } from '../home/home';
import {SignupPage} from '../signup/signup';
import {ForgotPasswordPage} from '../forgot-password/forgot-password';

declare var FCMPlugin;
/*
 Generated class for the Login page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'login-page',
  templateUrl: 'login.html'
})
export class LoginPage {
  error: any;
  form: any;
  token: any;

  constructor(public navCtrl: NavController, public auth: FirebaseAuth,
              public loadingCtrl: LoadingController, public session: Session, public events: Events) {
    // Get user data that loginned before, from localstorage
    let userLC = session.getUser();
    if (userLC) {
      this.form = userLC;
      // Auto Login
      // this.doLogin();
    } else {
      this.form = {
        cpf: '',
        password: ''
      }
    }
    if (typeof (FCMPlugin) !== "undefined") {
      FCMPlugin.getToken((token) => {
        console.log('token', token);
        if (token !== null) this.token = token;
      }, (error) => {
        console.log('error', error);
      });
    }
  }
  // Forgot Password
  openForgotPasswordPage(): void {
    this.navCtrl.push(ForgotPasswordPage);
  }
  // Signup
  openSignUpPage(): void {
    this.navCtrl.push(SignupPage);
  }
  // Cancel, Go to First page
  doCancel() {
    this.navCtrl.pop();
  }

  onSubmit($event:any){
    console.log("submit clicked");
    this.doLogin();
  }
  // Login
  doLogin() {
    this.error = '';
    if (this.form.cpf && this.form.password) {
      let loading = this.loadingCtrl.create();
      loading.present();
      this.auth.loginWithCPF(this.form).subscribe(data => {
        if (this.token) {
          this.auth.addFCMToken(this.form, this.token).subscribe(data=> {
            console.log(data);
          })
        }
        Toast.show('Login success', '3000', 'bottom');
        loading.dismiss();
        console.log('returnData', data);
        this.session.setUserId(data.uid);
        this.session.setUser(this.form);
        this.navCtrl.setRoot(HomePage, {'isLogin':true});
        this.events.publish('login:logined', true);
        // The auth subscribe method inside the app.ts will handle the page switch to home
      }, err => {
        loading.dismiss();
        console.log(err);
        if(typeof err == "string"){
          this.error = err;
        }else{
          this.error = err.message;
        }
      });
    } else {
      this.error = 'Insira CPF e senha.';
    }
  }
}
