import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';


@Component({
  selector: 'terms-of-service-page',
  templateUrl: 'terms-of-service.html'
})
export class TermsOfServicePage {

  hideHomeButton:any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.hideHomeButton = this.navParams.get('hidehomebutton');
    console.log(this.hideHomeButton);
  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }
}
