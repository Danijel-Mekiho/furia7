import {Component, ViewChild, ChangeDetectorRef} from '@angular/core';
import {NavController, NavParams, Content, AlertController, LoadingController} from 'ionic-angular';
import moment from 'moment';

import {EventEnquetPage} from '../event-enquet/event-enquet';
import {FirebaseData} from "../../providers/firebase-data";
import {BaseData} from "../../providers/base-data";
import {DomSanitizer} from "@angular/platform-browser";
import {FirebaseAuth} from "../../providers/firebase-auth";
import {SpeakerDetailPage} from "../speaker-detail/speaker-detail";
import { HomePage } from '../home/home';
import {Session} from "../../providers/session";

/*
  Generated class for the ProgramDetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-program-detail',
  templateUrl: 'program-detail.html'
})
export class ProgramDetailPage {
  @ViewChild(Content) content: Content;
  isScroll : boolean;
  lectureData: any;
  simObj: any;
  data:any;
  userid: any;
  eventType:any;
  setedSim:any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private cref: ChangeDetectorRef,
    public alertCtrl: AlertController,
    public fbauth: FirebaseAuth,
    public fb: FirebaseData,
    public bd: BaseData,
    public loadingCtrl: LoadingController,
    public sanitizer: DomSanitizer,
    public session: Session
  ) {
    this.userid = session.getUserId();
    let key = navParams.get('key');

    this.isScroll = true;
    this.simObj = {};
    let loading = this.loadingCtrl.create();
    loading.present();
    this.fb.object(`lectures/${key}`).subscribe(lecture => {
      this.data = lecture;

      this.simObj[lecture.$key] = false;
      this.fb.object(`events/${lecture['event_id']}`).subscribe(event => {
        let type = event.type;
        this.eventType = event.type;
        console.log('event', event);
        for (let key in lecture) {
          switch(key) {
            case 'speakers':
            case 'polls':
            case 'materials':
            case 'users':
            case 'vouchers': {
              if(lecture[key]) this.data[key] = this.bd.getObjectFromKey(key, lecture[key]);
              break;
            }
            case 'registered_doctor': {
              for (let k = 0; k < lecture[key].length; k++) {
                let _regDoc = lecture[key][k];
                if (this.userid == _regDoc) {
                  this.simObj[lecture.$key] = true;
                  break;
                }
              }
              break;
            }
            default: {
              //statements;
              break;
            }
          }
        }
        loading.dismiss();
      });

    });
  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  getValidTimeRange(key) {
    moment.locale('pt');
    return moment(key).format('LL');
  }

  getSafeUrl(url) {
    return (url)?this.sanitizer.bypassSecurityTrustResourceUrl(url): './assets/images/blank_user.jpg';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductPage');
  }


  gotoUsersList(speaker, key) {
    this.navCtrl.push(SpeakerDetailPage, {speaker: speaker, eventkey: key});
  }

  gotoEnquet (key, poll) {
    // console.log(polls);
    this.navCtrl.push(EventEnquetPage, {'lectureId': key, 'poll': poll});
  }

  goToDownload (material) {
    console.log(material);
    window.open(material.file, '_blank');
  }

  setSim(lecture) {
    console.log(lecture);
    if (!this.simObj[lecture.$key]) {
      let confirm = this.alertCtrl.create({
        title: 'CONFIRME',
        message: 'As inscrições para esse evento é limitado, por favor tenha certeza que comparecerá para não tirar uma vaga de alguma pessoa que realmente irá participar.',
        mode:'ios',
        buttons: [
          {
            text: 'NÃO',
            handler: () => {
              console.log('Agree clicked');
            }
          },
          {
            text: 'SIM',
            handler: () => {
              let serverData = this.fb.object(`lectures/${lecture.$key}/registered_doctor`);
              let sendData = lecture;
              let regDoctors = [];
              if (lecture.registered_doctor && lecture.registered_doctor.length) {
                regDoctors = lecture.registered_doctor;
              }
              regDoctors.push(this.userid);
              sendData.registered_doctor = regDoctors;
              console.log("regDoctors", regDoctors);
              serverData.update(regDoctors).then(firebaseNewData => {
                this.simObj[lecture.$key] = true;
                this.setedSim[lecture.$key] = true;
              }, error => {
                this.simObj[lecture.$key] = false;
              });
            }
          }
        ]
      });
      confirm.present();
    }else {
      let confirm = this.alertCtrl.create({
        title: 'CONFIRME',
        message: 'Você confirma que não poderá comparecer ao evento.',
        mode:'ios',
        buttons: [
          {
            text: 'NÃO',
            handler: () => {
              console.log('Agree clicked');
            }
          },
          {
            text: 'SIM',
            handler: () => {
              let serverData = this.fb.object(`lectures/${lecture.$key}/registered_doctor`);
              let sendData = lecture;
              let regDoctors = [];
              if (lecture.registered_doctor && lecture.registered_doctor.length) {
                regDoctors = lecture.registered_doctor;
                console.log('registered_doctor', lecture.registered_doctor);
                for (let i = 0; i < regDoctors.length; i++) {
                  let item = regDoctors[i];
                  if (item === this.userid){
                    regDoctors.splice(i, 1);
                    break;
                  }
                }
                sendData.registered_doctor = regDoctors || [];
                console.log("regDoctors", regDoctors);
                serverData.set(regDoctors).then(firebaseNewData => {
                  console.log('firebaseNewData', firebaseNewData);
                  this.simObj[lecture.$key] = false;
                  this.setedSim[lecture.$key] = false;
                }, error => {
                  console.log('firebaseNewerror', error);
                });
              }
            }
          }
        ]
      });
      confirm.present();
    }

  }


}
