import { Component } from '@angular/core';
import {NavController, LoadingController} from 'ionic-angular';

import { TabsNavigationPage } from '../tabs-navigation/tabs-navigation';
import {FirebaseAuth} from "../../providers/firebase-auth";
import {AngularFire} from "angularfire2";

@Component({
  selector: 'forgot-password-page',
  templateUrl: 'forgot-password.html'
})
export class ForgotPasswordPage {
  forgot_password: any;
  main_page: { component: any };
  error: any;

  constructor(public nav: NavController, public loadingCtrl: LoadingController, public af: AngularFire, public authFire: FirebaseAuth) {
    this.main_page = { component: TabsNavigationPage };

    this.forgot_password = {cpf: ''};
  }

  recoverPassword(){
    if(this.forgot_password.cpf){
      let loading = this.loadingCtrl.create();
      loading.present();
      let serverData = this.af.database.list('users', {
        query: {
          orderByChild: 'cpf',
          equalTo:this.forgot_password.cpf
        }
      });
      serverData.subscribe(snapshot => {
        console.log(snapshot);
        if(snapshot.length){
          let item = snapshot[0];
          this.authFire.sendPasswordResetEmail(item.email).subscribe(data => {
            loading.dismiss();
          }, error => {
            this.error = error;
            loading.dismiss();
          })
        } else {
          this.error = 'Este CPF ' + this.forgot_password.cpf + ' já está cadastrado na base de dados.';

          loading.dismiss();
        }

      });
    }
    // this.nav.setRoot(this.main_page.component);
  }

}
