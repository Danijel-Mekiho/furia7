import {Component, ViewChild, ChangeDetectorRef, ElementRef} from '@angular/core';
import {NavController, NavParams, Content, ViewController, LoadingController} from 'ionic-angular';
import {FirebaseData} from "../../providers/firebase-data";
import {FirebaseAuth} from "../../providers/firebase-auth";

import { HomePage } from '../home/home';
import {Session} from "../../providers/session";
import {AlertLoader} from "../../providers/alert-loader";

declare var google;
/*
  Generated class for the TravelAgency page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-travel-agency',
  templateUrl: 'travel-agency.html'
})
export class TravelAgencyPage {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild(Content) content: Content;
  map: any;
  isScroll : boolean;
  data: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private cref: ChangeDetectorRef,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController,
    public fb: FirebaseData,
    public fbauth: FirebaseAuth,
    public session: Session,
    public alertLoader:AlertLoader
  ) {
    let userId = session.getUserId();
    let eventId = this.navParams.get('key');
    this.isScroll = true;
    let loading = this.loadingCtrl.create();
    loading.present();
    fb.list('travels', {
      query: {
        orderByChild: 'event_id',
        equalTo: eventId
      }
    }).take(1).subscribe(travels => {

      for (let i = 0; i < travels.length; i++) {
        let item = travels[i];
        if (item.user_id === userId) {
          this.data = item;


          console.log('this.data', this.data);
          break;
        }
      }

      if (this.data) {
        setTimeout(_ => {
          this.loadMap();
          loading.dismiss();
        }, 500);
      } else {
        loading.dismiss();
        this.alertLoader.complexAlert('Informação não existente. A agência de viagem ainda não completou as suas informações, por favor aguarde mais alguns dias. OK', ()=>{
          this.navCtrl.pop();
        });
      }


      setTimeout(_ => {
        this.scrollChange();
      }, 500);
    });

  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  ionViewDidLoad() {

  }

  scrollTo(element:string) {
    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 500);
    this.scrollChange();
  }

  scrollChange () {
    let dementions = this.content.getContentDimensions();
    if(dementions.scrollTop > 100) {
      this.isScroll = true;
    } else {
      this.isScroll = false;
    }
    this.cref.detectChanges();
    setTimeout(_ => {
      this.content.resize();
    }, 120);
  }
  loadMap(){
    let latLng = new google.maps.LatLng(-23.539548, -46.596838);
    if (this.data.hotel_info) {
      latLng = new google.maps.LatLng(this.data.hotel_info.lat, this.data.hotel_info.lng);
    }
    let mapOptions = {
      center: latLng,
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    let marker = new google.maps.Marker({
      map: this.map,
      position: latLng,
      visible: true
    });
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
}
