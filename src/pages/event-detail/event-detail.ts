import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import moment from 'moment';

import { HomePage } from '../home/home';
import {EventInfoPage} from '../event-info/event-info';
import {ProgramingPage} from '../programing/programing';
import {UsersListPage} from '../users-list/users-list';
import {TravelAgencyPage} from '../travel-agency/travel-agency';
import {DomSanitizer} from "@angular/platform-browser";
/*
 Generated class for the EventDetail page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-event-detail',
  templateUrl: 'event-detail.html'
})
export class EventDetailPage {
  data:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private sanitizer: DomSanitizer) {
    this.data = this.navParams.get('item');
    console.log(this.data);
  }

  getSafeUrl(url) {
    if (url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    } else {
      return './assets/images/event-placehode.png';
    }
  }

  getValidTimeRange(key) {
    moment.locale('pt');
    return moment(key).format('LL');
  }

  goToInfo() {
    this.navCtrl.push(EventInfoPage, {item: this.data});
  }

  goToPro() {
    this.navCtrl.push(ProgramingPage, {key: this.data.$key, type: 'event'});
  }

  goToUsersList() {
    this.navCtrl.push(UsersListPage, {key: this.data.$key});
  }

  goToTravel() {
    this.navCtrl.push(TravelAgencyPage, {key: this.data.$key});
  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventDetailPage');
  }

}
