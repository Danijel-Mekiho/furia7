import {Component, ViewChild, ChangeDetectorRef} from '@angular/core';
import {NavController, NavParams, Content} from 'ionic-angular';
import { HomePage } from '../home/home';
/*
  Generated class for the Product page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-product',
  templateUrl: 'product.html'
})
export class ProductPage {
  @ViewChild(Content) content: Content;
  isScroll : boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, private cref: ChangeDetectorRef) {
    this.isScroll = true;
    setTimeout(_ => {
      this.scrollChange();
    }, 500);
  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductPage');
  }

  scrollTo(element:string) {
    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 500);
    this.scrollChange();
  }

  scrollChange () {
    let dementions = this.content.getContentDimensions();
    if(dementions.scrollTop > 100) {
      this.isScroll = true;
    } else {
      this.isScroll = false;
    }
    this.cref.detectChanges();
    setTimeout(_ => {
      this.content.resize();
    }, 120);

  }
}
