import {Component} from '@angular/core';
import {NavController, NavParams, ViewController, LoadingController} from 'ionic-angular';
import moment from 'moment';
import _ from 'lodash';

import {DomSanitizer} from "@angular/platform-browser";
import {FirebaseData} from "../../providers/firebase-data";
import {FirebaseAuth} from "../../providers/firebase-auth";
import {Session} from "../../providers/session";
import {Utilities} from "../../providers/utilities";


/*
 Generated class for the Promotion page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-promotion',
  templateUrl: 'promotion.html'
})
export class PromotionPage {
  data: any;
  userVouchers: any;
  userid: any;
  currentVoucher: any;

  constructor(public navCtrl: NavController,
              public viewCtrl: ViewController,
              public navParams: NavParams,
              public sanitizer: DomSanitizer,
              public fb: FirebaseData,
              public loadingCtrl: LoadingController,
              public authFire: FirebaseAuth,
              public session: Session,
              public utilities: Utilities) {

    this.currentVoucher = this.session.getVoucher();
    this.userid = session.getUserId();
    let loading = this.loadingCtrl.create();
    loading.present();

    //Get User Event Ids
    let eventIds = utilities.getCongressAndEventIdsByUserId(this.userid).subscribe(eventsIds => {

      //Get promotion data
      fb.list('promotion').take(1).subscribe(promotion => {
        console.log(promotion);
        loading.dismiss();

        let currentTimestamp = (new Date()).valueOf();
        console.log(currentTimestamp, promotion);

        let userPromotions = _.filter(promotion, (item:any)=>{
          if(this.currentVoucher == item.voucher || _.indexOf(eventsIds, item.event_id) != -1 || !item.event_id){
            if((!item.start_time || item.start_time <= currentTimestamp) && (!item.end_time || item.end_time >=  currentTimestamp)){
              return true;
            }else{
              return false;
            }

          }else{
            return false;
          }
        });

        let promotionAry = _.orderBy(userPromotions, ['create_at'], ['desc']);
        if(promotionAry.length > 0){
          this.data = promotionAry[0];
        }else{
          this.data = {};
        }
        console.log('data', userPromotions);
      });
    });
  }

  getSafeUrl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  getValidDate(key) {
    return moment(key).format('DD/MM/YYYY');
  }

  goToLink(link) {
    window.open(link, '_blank');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FileModalPage');
  }

}
