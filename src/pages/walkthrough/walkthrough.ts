import { Component, ViewChild } from '@angular/core';
import {NavController, Slides, Events, LoadingController} from 'ionic-angular';

import { LoginPage } from '../login/login';
import { SignupPage } from '../signup/signup';
import { HomePage } from '../home/home';
import {Session} from "../../providers/session";
import {FirebaseAuth} from "../../providers/firebase-auth";

declare var FCMPlugin;

@Component({
  selector: 'walkthrough-page',
  templateUrl: 'walkthrough.html'
})
export class WalkthroughPage {

  slide_options = {
    pager: true
  };
  lastSlide = false;
  token: any;
  form:any

  @ViewChild('slider') slider: Slides;

  constructor(
    public nav: NavController,
    public events: Events,
    public session: Session,
    public auth: FirebaseAuth,
    public loadingCtrl: LoadingController
  ) {
    this.form = session.getUserId() || false;
    this.auth.getAuth().subscribe((data)=>{
      if(data.uid){
        this.nav.setRoot(HomePage, {'isLogin':true});
      }
    })
  }

  skipIntro() {
    this.events.publish('login:logined', false);
    this.nav.setRoot(HomePage, {'isLogin':false});
  }

  onSlideChanged() {
    // If it's the last slide, then hide the 'Skip' button on the header
    this.lastSlide = this.slider.isEnd();
  }

  goToLogin() {
    this.nav.push(LoginPage);
  }

  goToSignup() {
    this.nav.push(SignupPage);
  }

  // Login
  doLogin() {
    let loading = this.loadingCtrl.create();
    loading.present();
    this.auth.loginWithCPF(this.form).subscribe(data => {
      loading.dismiss();
      this.session.setUser(this.form);
      this.nav.setRoot(HomePage, {'isLogin':true});
      this.events.publish('login:logined', true);
      // The auth subscribe method inside the app.ts will handle the page switch to home
    }, err => {
      loading.dismiss();
    });

  }
}
