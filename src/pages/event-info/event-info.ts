import {Component, ViewChild, ElementRef, ChangeDetectorRef} from '@angular/core';
import {NavController, ViewController, ModalController, Content, NavParams} from 'ionic-angular';
import { Geolocation } from 'ionic-native';
import moment from 'moment';

import { HomePage } from '../home/home';
import {DomSanitizer} from "@angular/platform-browser";
import { PathMapPage } from '../path-map/path-map';
declare var google;

/*
  Generated class for the EventInfo page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-event-info',
  templateUrl: 'event-info.html'
})
export class EventInfoPage {
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild(Content) content: Content;
  map: any;
  data: any;
  user: any;
  isScroll : boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public modalCtrl: ModalController, private cref: ChangeDetectorRef, private sanitizer: DomSanitizer) {
    this.data = this.navParams.get('item');
    this.isScroll = true;
    setTimeout(_ => {
      this.scrollChange();
    }, 1000);
  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  getSafeUrl(url) {
    if (url) {
      return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    } else {
      return './assets/images/event-placehode.png';
    }
  }

  getValidTimeRange(key) {
    moment.locale('pt');
    return moment(key).format('LL');
  }

  ionViewDidEnter(){
    this.loadMap();
  }

  scrollTo(element:string) {
    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 500);
    this.scrollChange();
  }

  scrollChange () {
    let dementions = this.content.getContentDimensions();
    if(dementions.scrollTop > 100) {
      this.isScroll = true;
    } else {
      this.isScroll = false;
    }
    this.cref.detectChanges();
    setTimeout(_ => {
      this.content.resize();
    }, 120);
  }

  loadMap(){
    if(!this.data.location || (!this.data.location.lat && this.data.location.lng)){
      return true;
    }
    let latLng = new google.maps.LatLng(this.data.location.lat, this.data.location.lng);
    let mapOptions = {
      center: latLng,
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    if (this.data.location.lat) {
      let marker = new google.maps.Marker({
        map: this.map,
        position: latLng,
        visible: true
      });
    }
  }
  showpathModal(data:any){
    Geolocation.getCurrentPosition().then(res => {
      let myLocation = {lat:res.coords.latitude, lng:res.coords.longitude};
      console.log({eventLocation:this.data.location, myLocation:myLocation});
      let modal = this.modalCtrl.create(PathMapPage, {eventLocation:this.data.location, myLocation:myLocation});
      modal.onDidDismiss(data => {
        console.log(data);
      });
      modal.present();
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
