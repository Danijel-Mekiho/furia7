import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

/*
  Generated class for the EventEnquetall page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-event-enquetall',
  templateUrl: 'event-enquetall.html'
})
export class EventEnquetallPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventEnquetallPage');
  }

}
