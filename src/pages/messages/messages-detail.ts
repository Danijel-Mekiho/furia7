import {Component, ViewChild, ElementRef, ChangeDetectorRef} from '@angular/core';
import {NavController, ViewController, Content, NavParams} from 'ionic-angular';
import {DomSanitizer} from "@angular/platform-browser";

/*
  Generated class for the Messages page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-message-detail',
  templateUrl: 'message-detail.html'
})
export class MessagesDetailPage {
  @ViewChild(Content) content: Content;
  data: any;
  user: any;
  isScroll : boolean;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private cref: ChangeDetectorRef,
    private sanitizer: DomSanitizer
  ) {
    this.data = this.navParams.get('item');
    console.log(this.data);
    this.isScroll = true;
    setTimeout(_ => {
      this.scrollChange();
    }, 1000);
  }

  getSafeUrl(url) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  scrollTo(element:string) {
    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 500);
    this.scrollChange();
  }

  scrollChange () {
    let dementions = this.content.getContentDimensions();
    if(dementions.scrollTop > 100) {
      this.isScroll = true;
    } else {
      this.isScroll = false;
    }
    this.cref.detectChanges();
    setTimeout(_ => {
      this.content.resize();
    }, 120);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagesPage');
  }

}
