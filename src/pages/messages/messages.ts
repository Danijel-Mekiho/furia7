import { Component } from '@angular/core';
import {NavController, NavParams, LoadingController, AlertController} from 'ionic-angular';
import moment from 'moment';
import _ from 'lodash';

import {MessagesDetailPage} from './messages-detail';
import {FirebaseData} from "../../providers/firebase-data";
import {ProgramDetailPage} from "../program-detail/program-detail";
import {Session} from "../../providers/session";
import { HomePage } from '../home/home';

/*
  Generated class for the Messages page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html'
})
export class MessagesPage {
  data: any;
  userid: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FirebaseData,
    public session: Session,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController
  ) {
    this.userid = session.getUserId();
    let loading = this.loadingCtrl.create();
    loading.present();
    this.fb.list('notifications').subscribe(snapshot => {

      let data = _.filter(snapshot, (item:any)=>{
        if(item.users && _.indexOf(item.users, this.userid) != -1){
          return true;
        }else{
          return false;
        }
      })

      data = _.orderBy(data, ['send_date'], ['desc']);

      let ary = [];
      _.forEach(data, (item:any)=>{
        if(item.is_confirm == "yes"){
          item.confirmed = true;
        }else{
          if(!item.confirmed_users){
            item.confirmed_users = [];
          }
          if(_.indexOf(item.confirmed_users, this.userid) != -1){
            item.confirmed = true;
          }else{
            item.confirmed = false;
          }
        }
        ary.push(item);
      })


      this.data = ary;
      loading.dismiss();
      console.log(this.data);
    });

  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  getValidTimeRange(key) {
    moment.locale('pt');
    return moment(key).format('LLL');
  }

  goToNotificationPage (item) {
    if (item.confirmed) {
      if (item.lecture_id) {
        if(item.notificaion_type == "check_in"){
          this.showCheckinNotification(item);
        }else{
          this.navCtrl.push(ProgramDetailPage, {'key': item.lecture_id});
        }
      } else {
        this.navCtrl.push(MessagesDetailPage, {item: item});
      }
    } else {
      let loading = this.loadingCtrl.create();
      loading.present();

      this.fb.object('notifications/' + item.$key).take(1).subscribe(sendData => {
        if(!sendData.confirmed_users){
          sendData.confirmed_users = [];
        }
        sendData.confirmed_users.push(this.userid);
        if(sendData.confirmed_users.length == sendData.users.length){
          sendData.is_confirm = 'yes';
        }

        this.fb.list('notifications').update(item.$key, sendData).then((data: any) => {
          loading.dismiss();
          if (item.lecture_id) {
            if(item.notificaion_type == "check_in"){
              this.showCheckinNotification(item);
            }else{
              this.navCtrl.push(ProgramDetailPage, {'key': item.lecture_id});
            }
          } else {
            this.navCtrl.push(MessagesDetailPage, {item: item});
          }
        });
      });
    }
  }


  showCheckinNotification(item){
    let lectureId = item.lecture_id;
    this.fb.object('lectures/'+lectureId).take(1).subscribe((lecture:any)=>{
      let alert = this.alertCtrl.create({
        title: 'Check In',
        subTitle: 'Você quer fazer o check in na palestra '+lecture.name+'?',
        mode:'ios',
        buttons: [
          {
            text: 'NÃO',
            role: 'cancel',
            handler: () => {
              if(!lecture.uncheck_in_doctor){
                lecture.uncheck_in_doctor = [];
              }
              if(!lecture.check_in_doctor){
                lecture.check_in_doctor = [];
              }
              if(lecture.check_in_doctor.indexOf(this.userid) != -1){
                lecture.check_in_doctor.splice(lecture.check_in_doctor.indexOf(this.userid), 1);
              }
              lecture.uncheck_in_doctor.push(this.userid);
              this.fb.update('lectures/'+lectureId, lecture);
              this.navCtrl.push(ProgramDetailPage, {'key': lectureId});
            }
          },
          {
            text: 'SIM',
            handler: () => {
              if(!lecture.check_in_doctor){
                lecture.check_in_doctor = [];
              }
              lecture.check_in_doctor.push(this.userid);
              this.fb.update('lectures/'+lectureId, lecture);
              this.navCtrl.push(ProgramDetailPage, {'key': lectureId});
            }
          }
        ]
      });
      alert.present();
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MessagesPage');
  }

}
