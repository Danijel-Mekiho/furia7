import {Component, ViewChild, ChangeDetectorRef} from '@angular/core';
import {NavController, NavParams, Content, LoadingController, AlertController} from 'ionic-angular';
import moment from 'moment';

import { HomePage } from '../home/home';
import {FirebaseData} from "../../providers/firebase-data";
import {AngularFire} from "angularfire2";
import {Session} from "../../providers/session";
import {BaseData} from "../../providers/base-data";
import {FirebaseAuth} from "../../providers/firebase-auth";


/*
 Generated class for the EventEnquet page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-event-enquet',
  templateUrl: 'event-enquet.html'
})
export class EventEnquetPage {
  @ViewChild(Content) content: Content;
  isScroll: boolean;
  data: any;
  lectureId: string;
  currentIndex: number;
  answers: any;
  userid: any;
  answerKey: any;
  lecture: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public session: Session,
    private cref: ChangeDetectorRef,
    private af: AngularFire,
    private loadingCtrl: LoadingController,
    public fbauth: FirebaseAuth,
    public bd: BaseData,
    public fb: FirebaseData,
    public alertCtrl: AlertController
  ) {
    let key = this.navParams.get('lectureId');
    let poll = this.navParams.get('poll');
    this.currentIndex = 0;
    console.log('poll', poll);
    this.isScroll = true;
    this.userid = session.getUserId();
    let loading = this.loadingCtrl.create();
    loading.present();
    this.fb.object(`lectures/${key}`).subscribe(lecture => {
      this.lecture = lecture;
      this.data = poll.questions;

      let answerRef = fb.list('poll_answers', {
        query: {
          orderByChild: 'user_id',
          equalTo: this.userid
        }
      });
      answerRef.take(1).subscribe(answers => {
        this.answers = answers;

        this.checkAnswerStatus();
      });



      loading.dismiss();
      // this.setAnswer();

      setTimeout(_ => {
        this.scrollChange();
      }, 1000);
    });
  }

  goHome () {
    this.navCtrl.setRoot(HomePage)
  }

  checkAnswerStatus () {
    let isAnsweredAlready = false;
    for(let i = 0; i < this.data.length; i++) {
      let item = this.data[i];
      for (let j = 0; j < this.answers.length; j++) {
        let answerItem = this.answers[j];
        if (item.question_id === answerItem.question_id) {
          isAnsweredAlready = true;
          break;
        }
      }
    }

    if (isAnsweredAlready) {
      let alert = this.alertCtrl.create({
        title: 'Já Completado',
        subTitle: 'Você já completou essa enquete, clique em voltar',
        mode:'ios',
        buttons: [{
          text: 'VOLTAR',
          handler: () => {
            this.navCtrl.pop();
          }
        }]
      });
      alert.present();
    }
  }

  getUnswerStatus(answer) {
    let returnVal = false;

    for (let i = 0; i < this.answers.length; i++) {
      let item = this.answers[i];
      if (item.answer_id === answer.answer_id) {
        returnVal = item.is_check;
        break;
      }
    }
    return returnVal;
  }

  getValidTimeRange(starttime, duration) {
    let endTime = starttime + duration*60*1000;
    return moment(starttime).format('D/MM/YY HH:mm') + '-' + moment(endTime).format('HH:mm');
  }

  getPrevTitle() {
    return 'VOLTAR';
  }

  getNextTitle() {
    if (this.currentIndex < this.data.length - 1) {
      return 'PRÓXIMO';
    } else {
      return 'Finalizar';
    }
  }

  changeCheckStatue(answer, question_item) {
    let isExist = false;
    let selectedItem = null;
    let tempUpdateCon = [];
    for (let i = 0; i < this.answers.length; i++) {
      let item = this.answers[i];

      if (question_item.is_single) {

        for (let k = 0; k < question_item.answers.length; k++) {
          let pa_answer_item = question_item.answers[k];
          if (item.answer_id === pa_answer_item.answer_id) {
            this.answers[i].is_check = false;
            let updateData = {
              user_id: this.userid,
              question_id: this.data[this.currentIndex].question_id,
              answer_id: this.answers[i].answer_id,
              is_check:false
            };
            if (answer.answer_id !== pa_answer_item.answer_id) {
              tempUpdateCon.push(this.af.database.list('poll_answers').update(this.answers[i].$key, updateData));
            }
          }
        }
        if (item.answer_id === answer.answer_id) {
          this.answers[i].is_check = true;
          isExist = true;
          let updateData = {
            user_id: this.userid,
            question_id: this.data[this.currentIndex].question_id,
            answer_id: this.answers[i].answer_id,
            is_check:true
          };
          tempUpdateCon.push(this.af.database.list('poll_answers').update(this.answers[i].$key, updateData));
        }
      } else {
        if (item.answer_id === answer.answer_id) {
          this.answers[i].is_check = !this.answers[i].is_check;
          isExist = true;
          selectedItem = this.answers[i];
          break;
        }
      }
    }

    let loading = this.loadingCtrl.create();
    loading.present();
    if (isExist){
      Promise.all(tempUpdateCon).then(_=> {
        loading.dismiss();
      });
    } else {
      let answerData = {
        user_id: this.userid,
        question_id: this.data[this.currentIndex].question_id,
        answer_id: answer.answer_id,
        is_check:true
      };
      this.answers.push(answerData);

      // this.bd.setPollAnswer(answerData);
      this.af.database.list('poll_answers').push(answerData).then((data: any) => {
        this.answers[(this.answers.length -1)].$key = data.key;
        loading.dismiss();
      });
    }
  }

  next() {
    if (this.currentIndex < this.data.length - 1) {
      this.currentIndex++;
    } else {
      let alert = this.alertCtrl.create({
        title: 'Enquete completa',
        subTitle: 'Obrigado Dr.(a) por finalizar a enquete, suas respostas foram enviadas com sucesso!',
        mode:'ios',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'OK',
            handler: () => {
              this.navCtrl.pop();
            }
          }
        ]
      });
      alert.present();
    }
  }

  previous() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
    }
  }

  scrollTo(element: string) {
    let yOffset = document.getElementById(element).offsetTop;
    this.content.scrollTo(0, yOffset, 500);
    this.scrollChange();
  }

  scrollChange() {
    let dementions = this.content.getContentDimensions();
    if (dementions.scrollTop > 100) {
      this.isScroll = true;
    } else {
      this.isScroll = false;
    }
    this.cref.detectChanges();
    setTimeout(_ => {
      this.content.resize();
    }, 120);
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad UsersListPage');
  }
}
