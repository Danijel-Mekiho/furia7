import {Injectable, Inject} from '@angular/core';
import {AngularFire, FirebaseApp} from 'angularfire2';

@Injectable()
export class FirebaseStorage {
  firebase: any;

  constructor(private af: AngularFire, @Inject(FirebaseApp) firebase: any) {
    this.firebase = firebase;
  }
  // Upload fire to the firebase cloud storage.
  upload(data: any) {
    let promise = new Promise((res, rej) => {
      let metaData = {
        contentType: 'image/jpeg'
      };

      let filename = this.getFilename(data);
      let genname = this.genFileName(filename);

      // Get base64 from file url
      this.convertToDataURLviaCanvas(data, 'image/jpeg').then(dataURL=>{
        let base64Str:any = dataURL;
        let re_data = base64Str.replace(/^data:image\/(png|jpeg);base64,/, "");

        // Convert base64 data to Blob type
        let file = this.base64ToBlob(re_data, metaData.contentType, 512);

        // Upload file to firebase cloud storage
        let storageRef = this.firebase.storage().ref();
        let uploadTask = storageRef.child(`images/${genname}`).put(file, metaData);
        uploadTask.on('state_changed', function (snapshot) {
        }, function (error) {
          rej(error);
        }, function () {
          let downloadURL = uploadTask.snapshot.downloadURL;
          let resdata = {
            imageId: genname,
            downloadUrl: downloadURL
          };
          res(resdata);
        });
      }, err=>{
        rej(err);
      });
    });
    return promise;
  }
  // Delete file to firebase cloud storage
  delete(path: string, data: any) {
    this.af.database.object(path).update(data);
  }
  // Convert base64 to Blob
  base64ToBlob(b64data, contentType, sliceSize) {
    let contenttype = (contentType) ? contentType : '';
    let slicesize = (sliceSize) ? sliceSize : 512;
    let byteString = atob(b64data);
    let byteArray = [];
    for (let offset = 0; offset < byteString.length; offset += slicesize) {
      let slice = byteString.slice(offset, offset + slicesize);
      let byteNumber = new Array(slice.length);
      for (let k = 0; k < slice.length; k++) byteNumber[k] = slice.charCodeAt(k);
      let byteArr = new Uint8Array(byteNumber);
      byteArray.push(byteArr);
    }
    return new Blob(byteArray, {type: contenttype});
  }

  genFileName(filename) {
    let s = '';
    let file_name = filename.replace('.jpg', '');
    file_name = file_name.replace('.jpeg', '');
    while (s.length < 20 && 20 > 0) {
      let r = Math.random();
      s += (r < 0.1 ? Math.floor(r * 100) : String.fromCharCode(Math.floor(r * 26) + (r > 0.5 ? 97 : 65)));
    }
    let dateString = new Date().getTime();
    return file_name + '_' + s + '_' + dateString.toString();
  }

  getFilepath(url) {
    let matchedurl = url.match(/^(.+\/)[^\/]+$/)[1];
    let n = matchedurl.search("file://");
    if (n < 0) matchedurl = 'file://' + matchedurl;
    return url.match(/^(.+\/)[^\/]+$/)[1];
  }

  getFilename(url) {
    return url.match(/^.+\/([^\/]+)$/)[1];
  }

  // Get Base64 data from file url
  convertToDataURLviaCanvas(url, outputFormat){
    return new Promise( (resolve, reject) => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = function(){
        let canvas = <HTMLCanvasElement> document.createElement('CANVAS'),
          ctx = canvas.getContext('2d'),
          dataURL;
        canvas.height = this.height;
        canvas.width = this.width;
        ctx.drawImage(this, 0, 0);
        dataURL = canvas.toDataURL("image/jpeg");
        //callback(dataURL);
        canvas = null;
        resolve(dataURL);
      };
      img.src = url;
    });
  }
}
