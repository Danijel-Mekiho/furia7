import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import {FirebaseData} from "./firebase-data";
import {LoadingController} from "ionic-angular";

/*
  Generated class for the BaseData provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class BaseData {

  SpeakerData: any;
  PollData: any;
  MaterialData: any;
  VoucherData: any;
  UsersData: any;
  PollAnswersData: any;

  speakers: any;
  polls: any;
  materials: any;
  vouchers: any;
  users: any;
  poll_answers: any;

  isLoaded: any;

  constructor(public fb: FirebaseData, public loadingCtrl: LoadingController) {
    this.SpeakerData = this.fb.list('speakers');
    this.PollData = this.fb.list('polls');
    this.MaterialData = this.fb.list('materials');
    this.UsersData = this.fb.list('users');
    this.VoucherData = this.fb.list('vouchers');
    this.PollAnswersData = this.fb.list('poll_answers');

    this.isLoaded = {
      speakers: false,
      polls: false,
      materials: false,
      users: false,
      vouchers: false,
      poll_answers: false
    }
  }

  _speakers() {
    return this.speakers;
  }

  _polls() {
    return this.polls;
  }

  _materials() {
    return this.materials;
  }

  _users() {
    return this.users;
  }

  _vouchers() {
    return this.vouchers;
  }

  _poll_answers() {
    return this.poll_answers;
  }

  getVouchers (){
    return this.vouchers;
  }

  getObjectFromKey(name, key) {
    let targetArr = [];
    let resultObj:any;
    switch(name) {
      case 'speakers': {
        targetArr = this.speakers;
        break;
      }
      case 'polls': {
        targetArr = this.polls;
        break;
      }
      case 'materials': {
        targetArr = this.materials;
        break;
      }
      case 'users': {
        targetArr = this.users;
        break;
      }
      case 'vouchers': {
        targetArr = this.vouchers;
        break;
      }
      case 'poll_answers': {
        targetArr = this.poll_answers;
        break;
      }
      default: {
        //statements;
        break;
      }
    }
    if(typeof (key) === 'string') {
      resultObj = null;
      for (let i = 0; i < targetArr.length; i++) {
        let item = targetArr[i];
        if (item.$key === key) {
          resultObj = item;
          break;
        }
      }
    } else {
      resultObj = [];
      for (let i = 0; i < key.length; i++) {
        let _key = key[i];
        for (let j = 0; j < targetArr.length; j++) {
          let item = targetArr[j];
          if (item.$key === _key) {
            resultObj.push(item);
            break;
          }
        }
      }
    }
    return resultObj;
  }

  setPollAnswer(data) {
    let isExist = false;
    let keyStr = data._id;
    for (let i = 0; i < this.poll_answers.length; i++) {
      let item = this.poll_answers[i];
      if(item._id === keyStr){
        this.poll_answers[i] = data;
        isExist = true;
        break;
      }
    }
    if (!isExist){
      this.poll_answers.push(data);
    }
  }

  getPollAnswer(userid, pollid) {
    let returnVal = null;
    for (let i = 0; i < this.poll_answers.length; i++) {
      let item = this.poll_answers[i];
      let keyStr = userid + '_' + pollid;
      if(item._id === keyStr){
        returnVal = item;
        break;
      }
    }
    return returnVal;
  }

  init () {
    this.SpeakerData.subscribe(speakers => {
      this.speakers = speakers;
    });
    this.PollData.subscribe(polls => {
      this.polls = polls;
    });
    this.MaterialData.subscribe(materials => {
      this.materials = materials;
    });
    this.UsersData.subscribe(users => {
      this.users = users;
    });
    this.VoucherData.subscribe(vouchers => {
      this.vouchers = vouchers;
    });
    this.PollAnswersData.take(1).subscribe(poll_answers => {
      this.poll_answers = poll_answers;
    });
  }

}
