import { Injectable } from '@angular/core';
import { AngularFire, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class FirebaseData {
  constructor(private af: AngularFire) {}
  // Add new data to the Firebase database object
  push(path: string, data: any): Observable<any> {
    return Observable.create(observer => {
      this.af.database.list(path).push(data).then(firebaseNewData => {
        // Return the uid created
        let newData: any = firebaseNewData;
        observer.next(newData.path.o[newData.path.o.length - 1]);
      }, error => {
        observer.error(error);
      });
    });
  }
  // Update data to the Firebase database object.
  update(path: string, data: any) {
    this.af.database.object(path).update(data);
  }
  // Get data list to the Firebase database object.
  list(path: string, query: any = {}): FirebaseListObservable<any> {
    return this.af.database.list(path, query);
  }
  // Get data object to the Firebase database object.
  object(path: string): FirebaseObjectObservable<any> {
    return this.af.database.object(path);
  }
  // Delete data object to the Firebase database object.
  remove(path: string): Observable<any> {
    return Observable.create(observer => {
      this.af.database.object(path).remove().then(data => {
        observer.next();
      }, error => {
        observer.error(error);
      });
    });
  }
}
