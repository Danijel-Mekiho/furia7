import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {LocalStorageService} from 'angular-2-local-storage';
import 'rxjs/add/operator/map';


declare var window;
/*
 Generated class for the Session provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class Session {
  USER_KEY = 'allergan-mobile-user';
  LOGIN_KEY = 'allergan-mobile-login';
  USERID_KEY = 'allergan-mobile-userid';
  FACEBOOK_TOKEN = 'allergan-mobile-fb-token';
  GOOGLE_TOKEN = 'allergan-mobile-google-token';
  ANSWER_POLL = 'allergan-mobile-poll-answer';
  VOUCHER_KEY = 'allergan-mobile-voucher';
  APP_FIRST = 'allergan-mobile-isfirst-';
  fb_token: any;
  google_token: any;
  user: any;
  userid: any;
  loginuser: any;
  // Store answers of Poll in Local
  poll_answer: any;
  voucher: any;
  isfirst: any;

  constructor(public http: Http, private localStrage: LocalStorageService) {
    let userLc: any = localStrage.get(this.USER_KEY);
    let useridLc: any = localStrage.get(this.USERID_KEY);
    let loginLc: any = localStrage.get(this.LOGIN_KEY);
    let fbtoken: any = localStrage.get(this.FACEBOOK_TOKEN);
    let googletoken: any = localStrage.get(this.GOOGLE_TOKEN);
    let poll_answerLC: any = localStrage.get(this.ANSWER_POLL);
    let voucherLC: any = localStrage.get(this.VOUCHER_KEY);
    let isfirstLC: any = window.localStorage[this.APP_FIRST+useridLc];
    this.user = JSON.parse(userLc) || false;
    this.userid = useridLc || false;
    this.loginuser = JSON.parse(loginLc) || false;
    this.fb_token = fbtoken || false;
    this.google_token = googletoken || false;
    // Get answers of Poll in LocalStrage
    this.poll_answer = JSON.parse(poll_answerLC) || false;
    this.voucher = voucherLC || false;
    this.isfirst = isfirstLC || false;
  }

  clear() {
    this.loginuser = this.user = this.fb_token = this.google_token = this.poll_answer = this.voucher = this.userid = false;
    this.localStrage.clearAll();
  }

  setUserId(data) {
    if (data) {
      this.userid = data;
      this.localStrage.set(this.USERID_KEY, data.toString());
    }
  }

  getUserId() {
    return this.userid;
  }

  setFirst() {
    this.isfirst = true;
    window.localStorage[this.APP_FIRST+this.userid] = this.isfirst;
  }

  getFirst() {
    return this.isfirst;
  }

  getFBToken() {
    return this.fb_token;
  }

  getGoogleToken() {
    return this.google_token;
  }

  setFBToken(token) {
    if (token) {
      this.fb_token = token;
      this.localStrage.set(this.FACEBOOK_TOKEN, token.toString());
    }
  }

  setGoogleToken(token) {
    if (token) {
      this.google_token = token;
      this.localStrage.set(this.GOOGLE_TOKEN, token.toString());
    }
  }

  setUser(data) {
    if (data) {
      this.user = data;
      this.localStrage.set(this.USER_KEY, JSON.stringify(data));
    }
  }

  getUser() {
    return this.user;
  }

  setVoucher(data) {
    if (data) {
      this.voucher = data;
      this.localStrage.set(this.VOUCHER_KEY, data);
    }
  }

  getVoucher() {
    return this.voucher;
  }

  setLogin(data) {
    if (data) {
      this.loginuser = data;
      this.localStrage.set(this.LOGIN_KEY, JSON.stringify(data));
    }
  }

  getLogin() {
    return this.loginuser;
  }

  setPollAnswers(data) {
    if (data) {
      this.poll_answer = data;
      this.localStrage.set(this.ANSWER_POLL, JSON.stringify(data));
    }
  }

  getPollAnswers() {
    return this.poll_answer;
  }
}
