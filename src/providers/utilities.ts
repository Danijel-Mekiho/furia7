import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import _ from 'lodash';

import {FirebaseData} from "./firebase-data";

@Injectable()
export class Utilities {

  constructor(public http: Http, public fb: FirebaseData) {

  }

  getEventIdsByUserId(userId): Observable<any>  {
    let defer = new Observable(observer=>{
      this.fb.list('events', {
        query: {
          orderByChild: 'type',
          equalTo: 0
        }
      }).take(1).subscribe(events => {
        let userEvents = _.filter(events, (event:any) => {
          let doctors = event.doctors || [];
          if(_.indexOf(doctors, userId) != -1){
            return true;
          }else{
            return false;
          }
        });
        let userEventIds = _.map(userEvents,(event:any) => {
          return event.$key
        })
        observer.next(userEventIds);
        observer.complete();
      });
    })
    return defer;
  }


  getCongressIds(): Observable<any>   {
    let defer = new Observable(observer=>{
      this.fb.list('events', {
        query: {
          orderByChild: 'type',
          equalTo: 1
        }
      }).take(1).subscribe(events => {
        let eventIds = _.map(events,(event:any) => {
          return event.$key
        })
        observer.next(eventIds);
        observer.complete();
      });
    })
    return defer;
  }

  getCongressAndEventIdsByUserId(userId): Observable<any>{
    let defer = new Observable(observer=>{
      this.fb.list('events').take(1).subscribe(events => {
        let userEvents = _.filter(events, (event:any) => {
          let doctors = event.doctors || [];
          if(_.indexOf(doctors, userId) != -1 || event.type == 1){
            return true;
          }else{
            return false;
          }
        });
        let userEventIds = _.map(userEvents,(event:any) => {
          return event.$key
        })
        observer.next(userEventIds);
        observer.complete();
      });
    })
    return defer;
  }

  validaCPF(cpf){
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;
    if (cpf.length < 11)
      return false;
    for (i = 0; i < cpf.length - 1; i++)
      if (cpf.charAt(i) != cpf.charAt(i + 1))
      {
        digitos_iguais = 0;
        break;
      }
    if (!digitos_iguais)
    {
      numeros = cpf.substring(0,9);
      digitos = cpf.substring(9);
      soma = 0;
      for (i = 10; i > 1; i--)
        soma += numeros.charAt(10 - i) * i;
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(0))
        return false;
      numeros = cpf.substring(0,10);
      soma = 0;
      for (i = 11; i > 1; i--)
        soma += numeros.charAt(11 - i) * i;
      resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
      if (resultado != digitos.charAt(1))
        return false;
      return true;
    }
    else
      return false;
  }

  validCRM(crm, uf): Observable<any> {
    return this.http.get('http://162.243.22.70/ws/consulta_crm.php?crm='+crm+'&uf=' + uf)
      .map(res => {
        return res.json()
      }, err => {
        return {}
      });
  }
}
