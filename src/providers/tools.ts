import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { UFData } from '../models/ufdata';
import {Session} from "./session";
import {FirebaseAuth} from "./firebase-auth";
/*
  Generated class for the Tools provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Tools {
  serverUrl: string;
  userid: string;
  constructor(public http: Http, public fbauth: FirebaseAuth) {
    this.serverUrl = 'https://us-central1-allergan-eventos.cloudfunctions.net/';
    console.log('Hello Tools Provider');
  }

  getUFData(): Observable<UFData> {
    return this.http.get('./assets/example_data/ufdata.json')
      .map(res => {
        return <UFData>(res.json());
      }, err => {
        return <UFData>({});
      });
  }

  confirmVoucher(voucher) {
    return this.http.get(this.serverUrl + 'confirmVoucher?voucher=' + voucher)
      .map(res => {
        console.log(res);
        return res;
      }, err => {
        console.log(err);
        return err;
      });
  }

  getUserVouchers() {

  }

}
