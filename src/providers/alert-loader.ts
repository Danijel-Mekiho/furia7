import { Injectable, Inject } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Injectable()
export class AlertLoader {
  constructor(public alertCtrl: AlertController){}
  showAlertMessage:boolean = false;
  complexAlert(subTitle, callback){
    if(!this.showAlertMessage){
      this.showAlertMessage = true;
      let Alert = this.alertCtrl.create({
        title: "Alerta",
        subTitle: subTitle,
        buttons: [
          {
            text: 'OK',
            role: 'ok',
            handler: () => {
              this.showAlertMessage = false;
              callback();
            }
          }
        ],
        enableBackdropDismiss:false
      })
      Alert.present();
    }
  }
}
