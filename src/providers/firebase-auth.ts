import {Injectable} from '@angular/core';
import {Platform} from 'ionic-angular';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {AngularFire, AuthProviders, AuthMethods} from 'angularfire2';
import firebase from 'firebase';
import {Facebook} from 'ionic-native';

import {FirebaseData} from './firebase-data';
import {Session} from "./session";

/*
 Generated class for the FirebaseAuth provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class FirebaseAuth {
  user: any;
  authResult: any;
  isauth: boolean;

  constructor(private af: AngularFire, private data: FirebaseData, private platform: Platform, public session: Session) {
    this.isauth = false;
    console.log(this.af.auth);
  }

  // Get logged in user infomation
  getAuthData() {
    return this.authResult;
  }

  // Get user infomation from key
  getAuth() {
    return Observable.create(observer => {
      this.af.auth.subscribe(authData => {
        if (authData) {
          observer.next(authData);
          observer.complete();
        } else {
          observer.next({});
          observer.complete();
        }
      });
    });
  }

  // Get user infomation from key
  getUserData() {
    return Observable.create(observer => {
      this.af.auth.subscribe(authData => {
        if (authData) {
          this.data.object('users/' + authData.uid).subscribe(userData => {
            this.user = userData;
            observer.next(userData);
            observer.complete();
          });
        } else {
          observer.error();
        }
      });
    });
  }

  // Add new user
  registerUser(credentials: any) {
    return Observable.create(observer => {
      // Check user duplication by cpf
      console.log('credentials', credentials);
      this.af.database.list('users', {
        query: {
          orderByChild: 'cpf',
          equalTo: credentials.cpf
        }
      }).take(1).subscribe(snapshot => {
        if (snapshot.length) {
          observer.error('Este CPF ' + snapshot[0].cpf + ' já está cadastrado na base de dados');
        } else {
          this.af.auth.createUser(credentials).then((authData: any) => {
            delete credentials.password;
            this.af.database.list('users').update(authData.uid, credentials).then((data: any) => {
              credentials.created = true;
              observer.next(credentials);
            });
          }, (error: any) => {
            if (error.message) {
              observer.error(error.message);
            } else {
              observer.error();
            }
          });
        }
      });

    });
  }

  addFCMToken(credentials: any, token: string) {
    return Observable.create(observer => {
      // Check user duplication by cpf
      console.log('credentials', credentials);
      this.af.database.list('users', {
        query: {
          orderByChild: 'cpf',
          equalTo: credentials.cpf
        }
      }).take(1).subscribe(snapshot => {
        if (snapshot.length) {
          let userData = snapshot[0];
          let isToken = false;
          userData.tokens = userData.tokens || [];
          for(let i = 0; i < userData.tokens.length; i++) {
            let item = userData.tokens[i];
            if (item === token) {
              isToken = true;
              break;
            }
          }
          if (!isToken){
            userData.tokens.push(token);
            this.af.database.list('users').update(userData.$key, userData).then((data: any) => {
              console.log('userData', data);
              observer.next(data);
            });
          } else {
            observer.error('This Token ' + snapshot[0].cpf + ' is already exist.');
          }
        } else {
          observer.error('This Token ' + snapshot[0].cpf + ' does not exist.');
        }
      });

    });
  }

  // Login by CPF
  loginWithCPF(credentials: any) {
    return Observable.create(observer => {
      this.af.database.list('users', {
        query: {
          orderByChild: 'cpf',
          equalTo: credentials.cpf
        }
      }).take(1).subscribe(snapshot => {
        console.log('snapshot', snapshot);
        if (snapshot.length) {
          let data = {email: snapshot[0].email, password: credentials.password};
          this.isAuthenticated().take(1).subscribe(authResp => {
            console.log('authResp', authResp);
            if (authResp) {
              console.log('authResp', authResp);
              console.log(authResp.auth.email, data.email);
              if (authResp.auth.email !== data.email) {
                this.af.auth.logout();
                this.af.auth.login(data, {
                  provider: AuthProviders.Password,
                  method: AuthMethods.Password
                }).then((authData) => {
                  this.authResult = authData;
                  console.log('login User', snapshot[0]);
                  this.session.setLogin(snapshot[0]);
                  observer.next(authData);
                }, (error) => {
                  observer.error(error);
                });
              } else {
                this.authResult = authResp.auth;
                observer.next(authResp.auth);
              }
            } else {
              this.af.auth.login(data, {
                provider: AuthProviders.Password,
                method: AuthMethods.Password
              }).then((authData) => {
                this.authResult = authData;
                console.log('login User', snapshot[0]);
                this.session.setLogin(snapshot[0]);
                observer.next(authData);
              }, (error:any) => {
                if(error.code == "auth/wrong-password"){
                  error.message = "O seu número de CPF ou senha é inválida, por favor insira novamente.";
                }
                observer.error(error);
              });
            }

            // observer.next(authResp);
          });
        } else {
          observer.error('O seu número de CPF ou senha é inválida, por favor insira novamente.');
        }

      }, error => {
        console.log('error', error);
      });

    });
  }

  // Login by Email
  loginWithEmail(credentials) {

    return Observable.create(observer => {
      this.isAuthenticated().take(1).subscribe(authResp => {
        console.log('isAuthenticated: ', authResp);
        if (authResp) {
          if (authResp.auth.email !== credentials.email) {
            this.af.auth.logout();
          }
        } else {
          this.af.auth.logout();
        }
        this.af.auth.login(credentials, {
          provider: AuthProviders.Password,
          method: AuthMethods.Password
        }).then((authData) => {
          this.authResult = authData;
          observer.next(authData);
        }, (error) => {
          observer.error(error);
        });
        // observer.next(authResp);
      });


    });
  }

  // Login by Facebook account
  loginWithFacebook() {
    return Observable.create(observer => {
      if (this.platform.is('cordova')) {
        Facebook.login(['public_profile', 'email']).then(facebookData => {
          let provider = firebase.auth.FacebookAuthProvider.credential(facebookData.authResponse.accessToken);
          firebase.auth().signInWithCredential(provider).then(firebaseData => {
            this.af.database.list('users').update(firebaseData.uid, {
              name: firebaseData.displayName,
              email: firebaseData.email,
              provider: 'facebook',
              image: firebaseData.photoURL
            });
            observer.next();
          });
        }, error => {
          observer.error(error);
        });
      } else {
        this.af.auth.login({
          provider: AuthProviders.Facebook,
          method: AuthMethods.Popup
        }).then((facebookData) => {
          this.af.database.list('users').update(facebookData.auth.uid, {
            name: facebookData.auth.displayName,
            email: facebookData.auth.email,
            provider: 'facebook',
            image: facebookData.auth.photoURL
          });
          observer.next();
        }, (error) => {
          observer.error(error);
        });
      }
    });
  }

  // Forgot password
  sendPasswordResetEmail(email) {
    return Observable.create(observer => {
      firebase.auth().sendPasswordResetEmail(email).then(function () {
        observer.next();
        // Email sent.
      }, function (error) {
        observer.error(error);
        // An error happened.
      });
    });
  }

  isAuthenticated(): Observable<any> {
    return this.af.auth; //auth is already an observable
  }

  // Logout
  logout() {
    this.af.auth.logout();
  }

}
