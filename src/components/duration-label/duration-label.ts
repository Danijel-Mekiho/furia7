import { Component, Input, ElementRef, Renderer, OnChanges } from '@angular/core';
import moment from 'moment';


@Component({
  selector: 'duration-label',
  template: `<p>{{label}}</p>`
})
export class DurationLabel implements OnChanges {

	constructor(public _elementRef: ElementRef, public _renderer: Renderer) {}

  @Input() start: string;
  @Input() end: string;
  label:any;

	ngOnChanges() {
    this._update();
  }

	_update() {
    let label = "";
    if(moment(this.start).format("YYYY") != moment(this.end).format("YYYY")){
      label = moment(this.start).format("LL") + " A " + moment(this.end).format("LL");
    }else if(moment(this.start).format("MM") != moment(this.end).format("MM")){
      label = moment(this.start).format("D [DE] MMMM") + " A " + moment(this.end).format("D [DE] MMMM") + " " +  moment(this.end).format("YYYY");
    }else{
      label = moment(this.start).format("D") + " A " + moment(this.end).format("D") + " DE " +  moment(this.end).format("MMMM [DE] YYYY");
    }
    this.label = label;
	}

}
